package com.company;

import java.awt.*;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
/**
 *       ________        _ _      _
 *      / / /  _ \      | | |    | |
 *     / / /| |_) |_   _| | | ___| |_
 *    / / / |  _ <| | | | | |/ _ \ __|
 *   / / /  | |_) | |_| | | |  __/ |_
 *  /_/_/   |____/ \__,_|_|_|\___|\__|
 *
 * Parent class handles the bullets to be created in game
 */
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
class Bullet {
	//-------------------------------------------------------------------------------------------//
    /**bullet position and size*/
    //-------------------------------------------------------------------------------------------//
    private int y = 0;
    private int x = 0;
	private int height = 10;
	private int width = 4;
	//-------------------------------------------------------------------------------------------//
	/**determines number of bullets per shot*/
	//-------------------------------------------------------------------------------------------//
    private int number = 1;
	//-------------------------------------------------------------------------------------------//
	/**damage that bullets will inflict*/
	//-------------------------------------------------------------------------------------------//
	private static int playerdamage = 100;
	//-------------------------------------------------------------------------------------------//
	//-------------------------------------------------------------------------------------------//
	/**boolean to set whether a bullet is able to pass through enemies and continue on*/
	//-------------------------------------------------------------------------------------------//
	private boolean moves_through = false;

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Get and Set methods*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public static int getPlayerdamage() { return playerdamage; }
    public static void setPlayerdamage(int playerdamage) { Bullet.playerdamage = playerdamage; }
    public static int getEnemydamage() {
		int enemydamage = 10;
		return enemydamage; }
	public void setXPos(int newx) { setX(newx); }
	public void setYPos(int newy) { setY(newy); }
	public int getY() { return y; }
	void setY(int y) { this.y = y; }
	int getX() { return x; }
	void setX(int x) { this.x = x; }
	int getPlayerbulletSpeed() { /*Sets players bullet speed*/
		int playerbulletSpeed = 1;
		return playerbulletSpeed; }
	public boolean isMoves_through() { return !moves_through; }
	void setMoves_through() { this.moves_through = true; }
	public int getNumber() { return number; }
	public void setNumber(int number) { this.number = number; }
	private int getHeight() { return height; }
	void setHeight(int height) { this.height = height; }
	int getWidth() { return width; }
	void setWidth(int width) { this.width = width; }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Sets outline of bullet for collision detection*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public Rectangle rect(boolean isPlayers) {
        if (isPlayers) {
            return new Rectangle(getX() + Player.getWidth() / 2 - getWidth() /2 + 8, Player.getY() + getY() - Player.getHeight(), getWidth(), getHeight());
        } else {
            return new Rectangle(getX(), getY(), getWidth(), getHeight());
        }
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**This method moves the bullet up based on the players bullet speed*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void moveBulletUp() {
        setY(getY() - getPlayerbulletSpeed() * 4);
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**This method moves the bullet down based on the enemies bullet speed*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void moveBulletDown() {
        int enemybulletSpeed = 1;
        setY(getY() + enemybulletSpeed * 4);
    }
}

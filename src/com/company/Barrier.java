package com.company;

import java.awt.*;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
/**       ________                  _
 *       / / /  _ \                (_)
 *      / / /| |_) | __ _ _ __ _ __ _  ___ _ __
 *     / / / |  _ < / _` | '__| '__| |/ _ \ '__|
 *    / / /  | |_) | (_| | |  | |  | |  __/ |
 *   /_/_/   |____/ \__,_|_|  |_|  |_|\___|_|
 *
 *  Class handles the barriers to be created in game
 **/
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

class Barrier {
	//-------------------------------------------------------------------------------------------//
    /**individual brick size*/
    //-------------------------------------------------------------------------------------------//
    private static final int brickLength = 4;
    private static final int brickHeight = 4;
	//-------------------------------------------------------------------------------------------//
    /**number of bricks in barrier*/
    //-------------------------------------------------------------------------------------------//
    private final int sectionsH = Screen.width / getBrickLength();
	//-------------------------------------------------------------------------------------------//
    /**array of barrierSections*/
    //-------------------------------------------------------------------------------------------//
    private final BarrierSection[][] barrier = new BarrierSection[getSectionsH()][getSectionsV()];

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Barrier Constructor*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public Barrier(int y) {

        for (int i = 0; i < getSectionsH(); i++) {
            for (int j = 0; j < getSectionsV(); j++) {
                getBarrier()[i][j] = new BarrierSection(i * getBrickLength(), y + j * getBrickHeight());
            }
        }
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Get and set methods*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public static int getBrickLength() { return brickLength; }
    private static int getBrickHeight() { return brickHeight; }
    public int getSectionsH() { return sectionsH; }
    public int getSectionsV() { return 10; }
    public BarrierSection[][] getBarrier() { return barrier; }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Class defining individual barrier pieces*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public class BarrierSection {
        final Rectangle shape;
        boolean cellIsDead = false;
        public BarrierSection(int x, int y) {
            shape = new Rectangle(x, y, getBrickLength(), getBrickHeight());
        }
    }
}

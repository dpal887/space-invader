package com.company;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

/**       _________  ____  _    _ _   _ _____    __  __          _   _          _____ ______ _____
 *       / / / ____|/ __ \| |  | | \ | |  __ \  |  \/  |   /\   | \ | |   /\   / ____|  ____|  __ \
 *      / / / (___ | |  | | |  | |  \| | |  | | | \  / |  /  \  |  \| |  /  \ | |  __| |__  | |__) |
 *     / / / \___ \| |  | | |  | | . ` | |  | | | |\/| | / /\ \ | . ` | / /\ \| | |_ |  __| |  _  /
 *    / / /  ____) | |__| | |__| | |\  | |__| | | |  | |/ ____ \| |\  |/ ____ \ |__| | |____| | \ \
 *   /_/_/  |_____/ \____/ \____/|_| \_|_____/  |_|  |_/_/    \_\_| \_/_/    \_\_____|______|_|  \_\
 *
 */

class SoundMgr implements Runnable {
    //---------------------------------------------//
    /**This checks if the music changes*/
    //---------------------------------------------//
    private int     doubleUpCheck;
    //---------------------------------------------//
    /**Placeholder clip to play the song*/
    //---------------------------------------------//
    private Clip    sound;
    //---------------------------------------------//
    /**
     * This sets the last position of the music so
     * that it continues
     */
    //---------------------------------------------//
    private int     lastpos = 0;
    /*****************************************END DECLARATIONS******************************************/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This is the main function for the sound manager. It repeats the song until the track changes*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    @Override
    public void run() {
        try {
            while (true) {
                doubleUpCheck = Main.getSoundtrack();
                //++++++++++++++++++++++++++++++++++++++++++++++++++//
                /*
                * Check for the music changing and if it changes change
                * the music
                */
                //++++++++++++++++++++++++++++++++++++++++++++++++++//
                switch (Main.getSoundtrack()) {
                    case 0:
                        break;
                    case 1:
                        Song1(new File("music\\Race_To_Action_10.wav"));
                        break;
                    case 2:
                        Song1(new File("music\\Strike_Eagle.wav"));
                        break;
                    case 3:
                        Song1(new File("music\\Race_To_Action.wav"));
                        break;
                    case 4:
                        Song1(new File("music\\Duplicitous.wav"));
                        break;
                    case 5:
                        sound.setFramePosition(lastpos);
                        Song1(new File("music\\NyanCat.wav"));
                }
                //++++++++++++++++++++++++++++++++++++++++++++++++++//
                /* Repeat until the music changes*/
                //++++++++++++++++++++++++++++++++++++++++++++++++++//
                while (Main.getSoundtrack() == doubleUpCheck) {
                    if (doubleUpCheck != 0) {
                        FloatControl gainControl = (FloatControl) sound.getControl(FloatControl.Type.MASTER_GAIN);
                        float soundControl = OptionsScreen.getSlider().getValue();
                        gainControl.setValue(soundControl);
                    }
                    Thread.sleep(1);
                }
                //++++++++++++++++++++++++++++++++++++++++++++++++++//
                /* Set the last position of the song*/
                //++++++++++++++++++++++++++++++++++++++++++++++++++//
                if(Main.getSoundtrack() == 5 || doubleUpCheck == 5){
                    lastpos = sound.getFramePosition();
                }
                //++++++++++++++++++++++++++++++++++++++++++++++++++//
                /* Close the sound if it is not track 0*/
                //++++++++++++++++++++++++++++++++++++++++++++++++++//
                if (doubleUpCheck != 0) {
                    sound.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function loads and plays the song selected in run function*/
    /**The song loops until a new song is selected*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private void Song1(File song) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        AudioInputStream audio = AudioSystem.getAudioInputStream(song);
        AudioFormat format = audio.getFormat();
        DataLine.Info info = new DataLine.Info(Clip.class, format);
        sound = (Clip) AudioSystem.getLine(info);
        sound.open(audio);
        sound.addLineListener(new LineListener() {
            public void update(LineEvent evt) {
                if (evt.getType() == LineEvent.Type.STOP) {
                    evt.getLine().close();
                    //++++++++++++++++++++++++++++++++++++++++++++++++++//
                    //Added this here to ensure music loops.
                    //++++++++++++++++++++++++++++++++++++++++++++++++++//
                    doubleUpCheck = -1;
                }
            }
        });
        sound.setFramePosition(lastpos);
        sound.start();
    }
}

package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *       ______  __  ____  _    _  _____ ______    ______      ________ _____
 *      / / /  \/  |/ __ \| |  | |/ ____|  ____|  / __ \ \    / /  ____|  __ \
 *     / / /| \  / | |  | | |  | | (___ | |__    | |  | \ \  / /| |__  | |__) |
 *    / / / | |\/| | |  | | |  | |\___ \|  __|   | |  | |\ \/ / |  __| |  _  /
 *   / / /  | |  | | |__| | |__| |____) | |____  | |__| | \  /  | |____| | \ \
 *  /_/_/   |_|  |_|\____/ \____/|_____/|______|  \____/   \/   |______|_|  \_\
 *  Highlights buttons when mouse over
 */

class MouseOver implements MouseListener {

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        JButton button = (JButton)e.getComponent();
        button.setBackground(new Color(0,0,255,100));
    }
    @Override
    public void mouseExited(MouseEvent e) {
        JButton button = (JButton)e.getComponent();
        button.setBackground(new Color(0, 0, 0, 0));
    }
}

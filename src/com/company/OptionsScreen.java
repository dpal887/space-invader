package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;

/**
 *      ________        _   _                _____
 *     / / / __ \      | | (_)              / ____|
 *    / / / |  | |_ __ | |_ _  ___  _ __   | (___   ___ _ __ ___  ___ _ __
 *   / / /| |  | | '_ \| __| |/ _ \| '_ \   \___ \ / __| '__/ _ \/ _ \ '_ \
 *  / / / | |__| | |_) | |_| | (_) | | | |  ____) | (__| | |  __/  __/ | | |
 * /_/_/   \____/| .__/ \__|_|\___/|_| |_| |_____/ \___|_|  \___|\___|_| |_|
 *               | |
 *               |_|
 *
 * Creates option screen to change the music channel
 */

class OptionsScreen extends JPanel {
	//-------------------------------------------------------------------------------------------//
	/**Initialises slider for music volume*/
	//-------------------------------------------------------------------------------------------//
    private static final JSlider slider = new JSlider(-50, 0, 0);
	//-------------------------------------------------------------------------------------------//
	/**Initialises background Image*/
	//-------------------------------------------------------------------------------------------//
	private static final String optionImage = "images\\optionBkg.png";
	private static Image optionBuffer;

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Get and Set methods*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	public static JSlider getSlider() {
		return slider;
	}
	private static String getOptionImage() {
		return optionImage;
	}
	private static Image getOptionBuffer() {
		return optionBuffer;
	}
	private static void setOptionBuffer(Image optionBuffer) {
		OptionsScreen.optionBuffer = optionBuffer;
	}

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Constructor for Options screen - deals with exceptions*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public OptionsScreen() throws IOException {
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    //adds background image
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
        setOptionBuffer(ImageIO.read(new File(getOptionImage())));
        setOptionBuffer(getOptionBuffer().getScaledInstance(Screen.width, (int) (Screen.height * 0.97), Image.SCALE_SMOOTH));
	    getSlider().setValue(0);
        this.setBackground(Color.black);
        this.setName("Screen Size Selection Panel");
        this.setLayout(new GridBagLayout());
        String[] comboBoxItems = {"1", "2", "3", "Off"};
        JComboBox<String> cb = new JComboBox<String>(comboBoxItems);
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    //Combo box selection for music track
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
        cb.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                String str = e.getItem().toString();

                if (str.substring(0, 1).equals("1")) {
                    Main.setSoundtrack(2);
                }
                if (str.substring(0, 1).equals("2")) {
                    Main.setSoundtrack(3);
                }
                if (str.substring(0, 1).equals("3")) {
                    Main.setSoundtrack(4);
                }
                if (str.substring(0, 1).equals("O")) {
                    Main.setSoundtrack(0);
                }
            }
        });
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    //Creates back button
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
        JButton button = new JButton(new ImageIcon("images/back.png"));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.getFlag()[1] = false;
                Main.getFlag()[0] = true;
            }
        });
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    //Sets layout of screen
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
        JLabel label = new JLabel();
        label.setFont(new Font("standard", Font.BOLD, 20));
        JLabel selectSound = new JLabel();
        selectSound.setFont(new Font("standard", Font.BOLD, 20));
        label.setText("Select a music track: ");
        label.setForeground(Color.blue);
        selectSound.setText("Change sound        : ");
        selectSound.setForeground(Color.blue);
        button.setBackground(new Color(0,0,0,0));
        button.setBorderPainted(false);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.1;
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(0, 100, 0, 0);
        this.add(label, c);
        c.insets = new Insets(0, 0, 0, 100);
        c.gridx = 1;
        this.add(cb, c);
        c.insets = new Insets(100, 100, 0, 0);
        c.gridx = 0;
        c.gridy = 1;
        this.add(selectSound, c);
        c.insets = new Insets(100, 0, 0, 100);
        c.gridx = 1;
        this.add(getSlider(), c);
        c.gridy = 2;
        c.gridx = 1;
        c.insets = new Insets(100, 0, 0, 100);
        this.add(button, c);
        getSlider().setMajorTickSpacing(10);
        getSlider().setMinorTickSpacing(1);
        getSlider().setPaintTicks(true);
        getSlider().setPaintLabels(true);
        getSlider().setForeground(new Color(0, 0, 255, 255));
        getSlider().setBackground(new Color(0, 0, 0, 0));
        cb.setForeground(new Color(0, 0, 0, 255));
        cb.setBackground(new Color(0, 0, 255, 200));
    }
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Function repaints screen*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(getOptionBuffer(), 0, 0, null);
    }
}

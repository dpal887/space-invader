package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
/**
 *      _________          __  __ ______    _____  _____ _____  ______ ______ _   _
 *     / / / ____|   /\   |  \/  |  ____|  / ____|/ ____|  __ \|  ____|  ____| \ | |
 *    / / / |  __   /  \  | \  / | |__    | (___ | |    | |__) | |__  | |__  |  \| |
 *   / / /| | |_ | / /\ \ | |\/| |  __|    \___ \| |    |  _  /|  __| |  __| | . ` |
 *  / / / | |__| |/ ____ \| |  | | |____   ____) | |____| | \ \| |____| |____| |\  |
 * /_/_/   \_____/_/    \_\_|  |_|______| |_____/ \_____|_|  \_\______|______|_| \_|
 *
 * Game screen creates the screen that the game is played on
 */
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

class GameScreen extends JPanel {

	//-------------------------------------------------------------------------------------------//
	/**power up array that holds each powerup on the screen*/
	//-------------------------------------------------------------------------------------------//
    private static final ArrayList<PowerUp> powerUp = new ArrayList<PowerUp>(0);                                              //powerUp arraylist
	//-------------------------------------------------------------------------------------------//
	/**power up array that holds every enemy on the screen*/
	//-------------------------------------------------------------------------------------------//
	private static final ArrayList<Enemy> enemy = new ArrayList<Enemy>(0);                                              //Create an enemy
	//-------------------------------------------------------------------------------------------//
	/**Set Nyan cat boolean*/
	//-------------------------------------------------------------------------------------------//
	private static boolean specialFlag = false;
	private static boolean nyn_flag = false;
	//-------------------------------------------------------------------------------------------//
	/**Initialise barracks and positions */
	//-------------------------------------------------------------------------------------------//
    private final Barrier barracks = new Barrier((int) (Screen.height - (Screen.height / 3)));                          //Create Barracks (x,y) --- Barrack is initialised at x position 0, (from side of screen) to y position of 1/3.2 times the JFrame height
    private final int firstBarrackStart = (Screen.width / Barrier.getBrickLength()) / 7;                                //Create the player images for standard, banking, and explosion.
    private final int firstBarrackEnd = (Screen.width / Barrier.getBrickLength()) / 7 * 2;                              //Set where the barracks start and end.         || [----------]          [-----------]         [----------] ||
    private final int secondBarrackStart = (Screen.width / Barrier.getBrickLength()) / 7 * 3;
    private final int secondBarrackEnd = (Screen.width / Barrier.getBrickLength()) / 7 * 4;
    private final int thirdBarrackStart = (Screen.width / Barrier.getBrickLength()) / 7 * 5;
    private final int thirdBarrackEnd = (Screen.width / Barrier.getBrickLength()) / 7 * 6;

	//-------------------------------------------------------------------------------------------//
	/**Boolean is true if player has died*/
	//-------------------------------------------------------------------------------------------//
    private boolean playerDead = false;                                                                                 //Used to signal to the main function when player is dead
	//-------------------------------------------------------------------------------------------//
	/**Initialise Images for player, background and nyan cat */
	//-------------------------------------------------------------------------------------------//
	private Image playerImage;                                                                                          //Create the player images for standard, banking, and explosion.
    private Image playerRightImage;
    private Image playerLeftImage;
    private Image backgroundImage;
    private Image nyan_cat;
	//-------------------------------------------------------------------------------------------//
	/**Initialise counter to set time of invincibility after life lost*/
	//-------------------------------------------------------------------------------------------//
    private int invicibilityCounter = 0;                                                                                //For future reference, sets player invincible if hit/power up for time specified below.
	//-------------------------------------------------------------------------------------------//
	/**Initialise nyan cat x position*/
	//-------------------------------------------------------------------------------------------//
	private static int nyn_x = 0;
	//-------------------------------------------------------------------------------------------//
	/**Shield Level*/
	//-------------------------------------------------------------------------------------------//
    private static int shieldActive = 1;
	//-------------------------------------------------------------------------------------------//
	/**Enemy Damage multiplier*/
	//-------------------------------------------------------------------------------------------//
    private static final double multi = 1.05;
	//-------------------------------------------------------------------------------------------//
	/**Initialise Level*/
	//-------------------------------------------------------------------------------------------//
    private static int level = 1;
	//-------------------------------------------------------------------------------------------//
	/**Set tutorial variables*/
	//-------------------------------------------------------------------------------------------//
    private static boolean mode = false;
    private static boolean tut1;
    private static boolean tut2;
    private static boolean tut3;
    private static boolean tut4;
    private static boolean tut5;
    private static boolean tut6;
    private static int rem_soundtrack;

	//-------------------------------------------------------------------------------------------//
	/**Get and Set Functions*/
	//-------------------------------------------------------------------------------------------//
    private Barrier getBarracks() {
        return barracks;
    }
    private int getFirstBarrackStart() {
        return firstBarrackStart;
    }
    private int getFirstBarrackEnd() {
        return firstBarrackEnd;
    }
    private int getSecondBarrackStart() {
        return secondBarrackStart;
    }
    private int getSecondBarrackEnd() {
        return secondBarrackEnd;
    }
    private int getThirdBarrackStart() {
        return thirdBarrackStart;
    }
    private int getThirdBarrackEnd() {
        return thirdBarrackEnd;
    }
    public boolean isPlayerDead() {
        return playerDead;
    }
    private void setPlayerDead(boolean playerDead) {
        this.playerDead = playerDead;
    }
    private Image getPlayerImage() {
        return playerImage;
    }
    private void setPlayerImage(Image playerImage) {
        this.playerImage = playerImage;
    }
    private Image getPlayerRightImage() {
        return playerRightImage;
    }
    private void setPlayerRightImage(Image playerRightImage) {
        this.playerRightImage = playerRightImage;
    }
    private Image getPlayerLeftImage() {
        return playerLeftImage;
    }
    private void setPlayerLeftImage(Image playerLeftImage) {
        this.playerLeftImage = playerLeftImage;
    }
    private Image getBackgroundImage() {
        return backgroundImage;
    }
    private void setBackgroundImage(Image backgroundImage) {
        this.backgroundImage = backgroundImage;
    }
    private Image getNyan_cat() {
        return nyan_cat;
    }
    private void setNyan_cat(Image nyan_cat) {
        this.nyan_cat = nyan_cat;
    }
    private int getInvicibilityCounter() {
        return invicibilityCounter;
    }
    private void setInvicibilityCounter(int invicibilityCounter) {
        this.invicibilityCounter = invicibilityCounter;
    }
    private static ArrayList<PowerUp> getPowerUp() {
        return powerUp;
    }
    public static ArrayList<Enemy> getEnemy() {
        return enemy;
    }
    private static boolean isSpecialFlag() {
        return specialFlag;
    }
    private static void setSpecialFlag(boolean specialFlag) {
        GameScreen.specialFlag = specialFlag;
    }
    private static int getNyn_x() {
        return nyn_x;
    }
    private static void setNyn_x(int nyn_x) {
        GameScreen.nyn_x = nyn_x;
    }
    private static boolean isNyn_flag() {
        return nyn_flag;
    }
    public static void setNyn_flag(boolean nyn_flag) {
        GameScreen.nyn_flag = nyn_flag;
    }
    private static int getShieldActive() {
        return shieldActive;
    }
    public static void setShieldActive(int shieldActive) {
        GameScreen.shieldActive = shieldActive;
    }
    private static double getMulti() {
        return multi;
    }
    public static int getLevel() {
        return level;
    }
    public static void setLevel(int level) {
        GameScreen.level = level;
    }
    public static boolean isMode() {
        return mode;
    }
    public static void setMode(boolean mode) {
        GameScreen.mode = mode;
    }
    private static boolean isTut1() {
        return tut1;
    }
    public static void setTut1(boolean tut1) {
        GameScreen.tut1 = tut1;
    }
    private static boolean isTut2() {
        return tut2;
    }
    public static void setTut2(boolean tut2) {
        GameScreen.tut2 = tut2;
    }
    private static boolean isTut3() {
        return tut3;
    }
    public static void setTut3(boolean tut3) {
        GameScreen.tut3 = tut3;
    }
    private static boolean isTut4() {
        return tut4;
    }
    public static void setTut4(boolean tut4) {
        GameScreen.tut4 = tut4;
    }
    private static boolean isTut5() {
        return tut5;
    }
    public static void setTut5(boolean tut5) {
        GameScreen.tut5 = tut5;
    }
    private static boolean isTut6() {
        return tut6;
    }
    public static void setTut6(boolean tut6) {
        GameScreen.tut6 = tut6;
    }
    private static int getRem_soundtrack() {
        return rem_soundtrack;
    }
    private static void setRem_soundtrack(int rem_soundtrack) {
        GameScreen.rem_soundtrack = rem_soundtrack;
    }

	//-------------------------------------------------------------------------------------------//
	/**Function initialises enemys*/
	//-------------------------------------------------------------------------------------------//
    public static void enemyInit() {
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    //Create each enemy and add to enemy array
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    for (int j = 0; j < 6; j++) {
            for (int i = 0; i < 6; i++) {
                addEnemy(i, j);
            }
        }
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    //sets enemys health depending on level
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
        for (Enemy anEnemy : getEnemy()) {
            anEnemy.setHealth((anEnemy.getHealth() + getLevel() * 50));
        }
    }

	//-------------------------------------------------------------------------------------------//
	/**This function works with the enemy init function to create all of the enemies*/
    //-------------------------------------------------------------------------------------------//
    private static void addEnemy(int EnemyX, int EnemyY) {

        Enemy.setSpeed(10);
        Enemy en1 = new Enemy();
        en1.setX(en1.getX() + Screen.width / 15 * EnemyX);
        en1.setY(en1.getY() + Screen.height / 15 * EnemyY);

        getEnemy().add(en1);
    }

	//-------------------------------------------------------------------------------------------//
	/**This function saves the game to a txt file*/
	//-------------------------------------------------------------------------------------------//
    public static void saveState() throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter("save.txt", "UTF-8");
        writer.println("Score:  " + Player.getScore());
        writer.println("Level:  " + getLevel());
        writer.println("Speed:  " + Player.getUpgradeSpeed());
        writer.println("FRate:  " + Player.getUpgradeFireRate());
        writer.println("Shield: " + Player.getShield());
        writer.println("Weapon: " + Player.getWeapon_type());
        writer.close();
    }

	//-------------------------------------------------------------------------------------------//
	/**This function loads the game from a txt file*/
	//-------------------------------------------------------------------------------------------//
    public static void loadState() throws IOException {
        FileReader reader = new FileReader("save.txt");
        BufferedReader buff = new BufferedReader(reader);
        String line;
        while ((line = buff.readLine()) != null) {
            if (line.substring(0, 5).equals("Score")) {
                Player.setScore(Integer.parseInt(line.substring(8, line.length())));
            }
            if (line.substring(0, 5).equals("Level")) {
                setLevel(Integer.parseInt(line.substring(8, line.length())));
            }
            if (line.substring(0, 5).equals("Speed")) {
                Player.setUpgradeSpeed(Double.parseDouble(line.substring(8, line.length())));
            }
            if (line.substring(0, 5).equals("FRate")) {
                Player.setUpgradeFireRate(Double.parseDouble(line.substring(8, line.length())));
            }
            if (line.substring(0, 6).equals("Shield")) {
                Player.setShield(Integer.parseInt(line.substring(8, line.length())));
            }
            if (line.substring(0, 6).equals("Weapon")) {
                Player.setWeapon_type(Integer.parseInt(line.substring(8, line.length())));
            }
        }
        buff.close();
        setShieldActive(Player.getShield());
        Player.reinitialiseStats();
    }
	//-------------------------------------------------------------------------------------------//
	/**This function initialises the gamescreen by setting the layout and adding all of the IO files*/
	//-------------------------------------------------------------------------------------------//
    GameScreen() throws IOException {

        this.setLayout(new GridBagLayout());
        String background = "images\\Background.jpg";
        setBackgroundImage(ImageIO.read(new File(background)));
        String playerImageFile = "images\\player.png";
        setPlayerImage(ImageIO.read(new File(playerImageFile)));
        String playerImageLeftFile = "images\\playerl.png";
        setPlayerLeftImage(ImageIO.read(new File(playerImageLeftFile)));
        String playerImageRightFile = "images\\playerr.png";
        setPlayerRightImage(ImageIO.read(new File(playerImageRightFile)));
        String nc = "images\\nyan_cat.gif";
        setNyan_cat(ImageIO.read(new File(nc)));
        setNyan_cat(getNyan_cat().getScaledInstance(Screen.width / 20, Screen.height / 20, Image.SCALE_SMOOTH));
        Thread sheildtime = new Thread(new ShieldTimer());
        sheildtime.start();
    }

	//-------------------------------------------------------------------------------------------//
	/**Function rebuilds barracks at the beginning of each level */
	//-------------------------------------------------------------------------------------------//
    private void renew_barracks() {
        for (int i = 0; i < getBarracks().getSectionsH(); i++) {
            for (int j = 1; j < getBarracks().getSectionsV(); j++) {
                getBarracks().getBarrier()[i][j].cellIsDead = false;
            }
        }
    }
	//-------------------------------------------------------------------------------------------//
	/**This function is used to reset all needed variables when one game is finished (or in between games)*/
	//-------------------------------------------------------------------------------------------//
	public void reset() {
		//Set player at center of screen
		Player.setX(Screen.width / 2);
		//Remove all bullets on the screen
		for (int i = 0; i < Player.getActiveBullets().size(); i++) {
			Player.getActiveBullets().removeAll(Player.getActiveBullets());
		}
		for (int i = 0; i < getPowerUp().size(); i++) {
			getPowerUp().removeAll(getPowerUp());
		}
		Player.setPowerUpSpeed(0.1);
		Player.setPowerUpFireRate(1.0);
		Player.reinitialiseStats();
		Player.setHealth(100);
		Player.setLives(3);
		setNyn_flag(false);
		setPlayerDead(false);
		getEnemy().removeAll(getEnemy());
		renew_barracks();
	}
	//-------------------------------------------------------------------------------------------//
    /**Paints everything the gamescreen needs. Player, enemy, barracks, bullets*/
	//-------------------------------------------------------------------------------------------//
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.red);
        //Set background image/color... for later use. Currently this is just a black screen
        g2.drawImage(getBackgroundImage(), 0, 0, null);
        this.setBackground(Color.BLACK);
        //Draw all of the health/score stuff
        g2.setColor(Color.WHITE);
        g2.setFont(Player.getPlayerFont());
        g2.drawString("Health : ", 10, 20);
        g2.drawString("Lives  : " + Player.getLives(), 10, 38);
        g2.drawString("Score  : " + Player.getScore(), 10, 56);
        g2.drawString("Level  : " + getLevel(), 10, 76);
        //Healthbar Drawing
        g2.fillRect(75, 8, 106, 12);
        if (Player.getHealth() < 25) {
            g2.setColor(new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255)));
        } else {
            g2.setColor(Color.RED);
        }
        g2.fillRect(79, 12, Player.getHealth(), 6);
        g2.setColor(Color.red);
        /** Handles Survival mode*/
        if (isMode()){
            Player.setWeapon_type(2);
            Bullet.setPlayerdamage(50);
            if (Player.getFireRate() > 1.5){
                Player.setFireRate(1.5);
            }
            if (Player.getSpeed() > 2){
                Player.setSpeed(2);
                for (Enemy anEnemy : getEnemy()) {
                    if (anEnemy.getHealth() > 240) {
                        anEnemy.setHealth(240);
                    }
                }
            }
        }
        if ((isMode()) && (Main.isSpawn())) {
            for (int y = 0; y < 1; y++) {
                for (int x = 0; x < 6; x++) {
                    addEnemy(x+1 , y);
                }
                for (Enemy anEnemy : getEnemy()) {
                    anEnemy.setHealth(anEnemy.getHealth() + 20);
                }
            }
            Main.setSpawn(false);
            Main.setStart(Main.getStart() + 1);
        }

        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        /**Draws and handles Enemy */
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        for (Enemy anEnemy : getEnemy()) {
            anEnemy.update();
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
            //Check if enemy has hit the bottom. If true then kill player.
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
            if ((Player.rect()).intersects(new Rectangle(anEnemy.getX(), anEnemy.getY() + Enemy.getHeight(), Enemy.getWidth(), Enemy.getHeight())) || anEnemy.getY() + Enemy.getHeight() > Screen.height) {
                setPlayerDead(true);
            }
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
            //Draw the enemy.
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
            g2.drawImage(anEnemy.getEnemyImage(), anEnemy.getX(), anEnemy.getY(), null);
            //Draw bullets and move bullets

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
            /**Draws and handles enemy bullets */
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
            for (int j = 0; j < anEnemy.getActiveBullets().size(); j++) {
                boolean enemyBulletSuccess = false;
                if (anEnemy.getActiveBullets().get(j).getY() < Screen.height) {
                    g2.setColor(Color.green);
                    g2.fill(anEnemy.getActiveBullets().get(j).rect(false));
                    anEnemy.getActiveBullets().get(j).moveBulletDown();
                } else {
                    anEnemy.getActiveBullets().remove(j);
                    break;
                }
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
                //Check for enemy bullet hits barracks collision
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
                for (int k = 0; k < getBarracks().getBarrier().length; k++) {
                    for (int l = 0; l < getBarracks().getBarrier()[k].length; l++) {
                        if (!getBarracks().getBarrier()[k][l].cellIsDead) {
                            if (anEnemy.getActiveBullets().get(j).rect(false).intersects(getBarracks().getBarrier()[k][l].shape)) {
                                anEnemy.getActiveBullets().remove(j);
                                getBarracks().getBarrier()[k][l].cellIsDead = true;
                                int n = 0;
                                while (Math.random() > 0.10){
                                    n++;
                                    if(k - n > 0 && k + n < getBarracks().getSectionsV()) {
                                        if (Math.random() > 0.90) {
                                            getBarracks().getBarrier()[j + n][k - n].cellIsDead = true;
                                        }
                                        if (Math.random() < 0.90) {
                                            getBarracks().getBarrier()[j - n][k + n].cellIsDead = true;
                                        }
                                        if (Math.random() > 0.90) {
                                            getBarracks().getBarrier()[j + n][k + n].cellIsDead = true;
                                        }
                                        if (Math.random() < 0.90) {
                                            getBarracks().getBarrier()[j - n][k - n].cellIsDead = true;
                                        }
                                    }
                                }
                                enemyBulletSuccess = true;
                                break;
                            }
                        }
                    }
                    if (enemyBulletSuccess) {
                        break;
                    }
                }
                if (enemyBulletSuccess) {
                    break;
                }
                g2.setColor(Color.RED);
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
                //Check for enemy hits player
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
                if (anEnemy.getActiveBullets().get(j).rect(false).intersects(new Rectangle(Player.getX(), Player.getY(), Player.getWidth(), Player.getHeight())) && !Player.isInvincible()) {
                    if (getShieldActive() <= 0) {
                        Player.decreaseHealth((int) (Bullet.getEnemydamage() * getMulti() + getLevel()));
                        anEnemy.getActiveBullets().remove(j);
                    } else {
                        setShieldActive(getShieldActive() - 1);
                        anEnemy.getActiveBullets().remove(j);
                    }
                    break;
                }
            }
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
        //Set speed and length of player flash when death occurs.
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
        int flashSpeed = 35;
        int flashLength = 500;

        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        /**Draws and handles player shield */
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        if (getShieldActive() > 0) {
            if(getShieldActive() * 10 < 255) {
                g2.setColor(new Color(getShieldActive() * 10, 0, 255, 255));
            }
            else if(getShieldActive() < 255){
                g2.setColor(new Color(255,getShieldActive(),255));
            }
            g2.fillOval(Player.getX() - 5, Screen.height - 150, 50, 50);
            g2.setColor(Color.red);
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        /**Draws and handles player */
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        if (Player.isInvincible()) {
            if ((getInvicibilityCounter() / flashSpeed) % 2 == 0) {
                g2.drawImage(getPlayerImage(), Player.getX(), Screen.height - 150, null);
                setInvicibilityCounter(getInvicibilityCounter() + 1);
            } else {
                setInvicibilityCounter(getInvicibilityCounter() + 1);
            }
            if (getInvicibilityCounter() > flashLength) {
                Player.setInvincible(false);
                setInvicibilityCounter(0);
            }
        } else if (Player.isTurnLeft()) {
            g2.drawImage(getPlayerLeftImage(), Player.getX(), Screen.height - 150, null);
        } else if (Player.isTurnRight()) {
            g2.drawImage(getPlayerRightImage(), Player.getX(), Screen.height - 150, null);
        } else {
            g2.drawImage(getPlayerImage(), Player.getX(), Screen.height - 150, null);
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        /**Draws and handles player bullets*/
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        for (int i = 0; i < Player.getActiveBullets().size(); i++) {
            boolean bulletSuccess = false;
            g2.setColor(new Color(255, 255, (int) (Math.random() * 255)));
            g2.fill(Player.getActiveBullets().get(i).rect(true));
            g2.setColor(Color.red);
            Player.getActiveBullets().get(i).moveBulletUp();
            for (int j = 0; j < getBarracks().getBarrier().length; j++) {
                for (int k = 0; k < getBarracks().getBarrier()[j].length; k++) {
                    if (!getBarracks().getBarrier()[j][k].cellIsDead) {
                        if (Player.getActiveBullets().get(i).rect(true).intersects(getBarracks().getBarrier()[j][k].shape)) {
                            Player.getActiveBullets().remove(i);
                            getBarracks().getBarrier()[j][k].cellIsDead = true;
                            int n = 0;
                            while (Math.random() > 0.10){
                                n++;
                                if(k - n > 0 && k + n < getBarracks().getSectionsV()) {
                                    if (Math.random() > 0.25) {
                                        getBarracks().getBarrier()[j + n][k - n].cellIsDead = true;
                                    }
                                    if (Math.random() < 0.25) {
                                        getBarracks().getBarrier()[j - n][k + n].cellIsDead = true;
                                    }
                                    if (Math.random() > 0.25) {
                                        getBarracks().getBarrier()[j + n][k + n].cellIsDead = true;
                                    }
                                    if (Math.random() < 0.25) {
                                        getBarracks().getBarrier()[j - n][k - n].cellIsDead = true;
                                    }
                                }
                            }
                            bulletSuccess = true;
                            break;
                        }
                    }
                }
                if (bulletSuccess) {
                    break;
                }
            }
            if (bulletSuccess) {
                break;
            }
            if (Player.getActiveBullets().get(i).rect(true).intersects(new Rectangle(getNyn_x(), 10, Screen.width / 20, Screen.height / 20)) && !isNyn_flag()) {
                Player.setScore(Player.getScore() + 50 * getLevel());
                setNyn_flag(true);
            }
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
            //Check for player bullet hits enemy.
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
            for (int j = 0; j < getEnemy().size(); j++) {
                if (Player.getActiveBullets().get(i).rect(true).intersects(new Rectangle(getEnemy().get(j).getX(), getEnemy().get(j).getY(), Enemy.getWidth(), Enemy.getHeight()))) {
                    getEnemy().get(j).setHealth((getEnemy().get(j).getHealth() - Bullet.getPlayerdamage()));
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
                    //Sets enemy outline colour based on health.
                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
                    if (getEnemy().get(j).getHealth() >= 50 && getEnemy().get(j).getHealth() <= 100) {
                        getEnemy().get(j).setN(2);
                    }
                    if (getEnemy().get(j).getHealth() >= 101 && getEnemy().get(j).getHealth() <= 150) {
                        getEnemy().get(j).setN(3);
                    }
                    if (getEnemy().get(j).getHealth() >= 151 && getEnemy().get(j).getHealth() <= 200) {
                        getEnemy().get(j).setN(4);
                    }
                    if (getEnemy().get(j).getHealth() >= 201 && getEnemy().get(j).getHealth() <= 250) {
                        getEnemy().get(j).setN(5);
                    }
                    if (getEnemy().get(j).getHealth() >= 251) {
                        getEnemy().get(j).setN(6);
                    }
                    if (getEnemy().get(j).getHealth() > 0 && getEnemy().get(j).getHealth() < 50) {
                        getEnemy().get(j).setN(1);
                    }
                    if (getEnemy().get(j).getHealth() <= 0) {
                        if ((Math.random() * 10 <= 2)&& (!isMode())) {
                            PowerUp power = new PowerUp((int) (Math.random() * 5 + 1));
                            power.setX(getEnemy().get(j).getX() + Enemy.getWidth() / 2 - power.getWidth() / 2);
                            power.setY(getEnemy().get(j).getY() + Enemy.getHeight());
                            power.createRect();
                            getPowerUp().add(0, power);
                        }
                        if ((Math.random() * 10 < 1) && (isMode())){
                            PowerUp power = new PowerUp((int) (Math.random() * 5 + 1));
                            power.setX(getEnemy().get(j).getX() + Enemy.getWidth() / 2 - power.getWidth() / 2);
                            power.setY(getEnemy().get(j).getY() + Enemy.getHeight());
                            power.createRect();
                            getPowerUp().add(0, power);
                        }
                        Player.setScore(Player.getScore() + getLevel() * 10);
                        if (getEnemy().size() < 9 + getLevel()) {
                            if (Enemy.getSpeed() > 1) {
                                Enemy.setSpeed(Enemy.getSpeed() - 1);
                            }
                        }
                        getEnemy().remove(j);
                        if (Player.getActiveBullets().get(i).isMoves_through()) {
                            Player.getActiveBullets().remove(i);
                            bulletSuccess = true;
                        }
                        break;
                    } else {
                        if (Player.getActiveBullets().get(i).isMoves_through()) {
                            Player.getActiveBullets().remove(i);
                            bulletSuccess = true;
                        }
                        break;
                    }
                }
            }
            if (!bulletSuccess) {
                if (Player.getActiveBullets().get(i).getY() < -Player.getY()) {
                    Player.getActiveBullets().remove(i);
                    break;
                }
            }
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        /**Draws and handles barracks */
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        for (int i = 0; i < getBarracks().getSectionsH(); i++) {
            for (int j = 0; j < getBarracks().getSectionsV(); j++) {
                if (!getBarracks().getBarrier()[i][j].cellIsDead) {
                    g2.setColor(new Color(0,255,0,(int)(Math.random() * 155)+100));
                    g2.fill(getBarracks().getBarrier()[i][j].shape);
                }
                if (i < getFirstBarrackStart() || (i > getFirstBarrackEnd() && i < getSecondBarrackStart()) || (i > getSecondBarrackEnd() && i < getThirdBarrackStart()) || (i > getThirdBarrackEnd())) {
                    if (Math.random() < 0.1) {
                        getBarracks().getBarrier()[i][j].cellIsDead = true;
                    }
                }
            }
        }
        //Check if player health is 0, then make player invincible and flash for a bit
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        /**Handles Player Loss of Life */
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        if (Player.getHealth() <= 0) {
            Player.setLives(Player.getLives() - 1);
            Player.heal();
            Player.setInvincible(true);
            Player.setX(Screen.width / 2);
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        /**Handles Player runs out of lives */
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        //Kill the player if lives go below 0
        if (Player.getLives() < 0) {
            setPlayerDead(true);
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        /**Draw and handle power ups*/
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        for (PowerUp aPowerUp : getPowerUp()) {
            if (aPowerUp.getY() >= Screen.height) {
                getPowerUp().remove(aPowerUp);
                break;
            }
            switch (aPowerUp.getPowerNumber()){
                case 1:                g2.setColor(Color.CYAN);
                    break;
                case 2:                g2.setColor(Color.ORANGE);
                    break;
                case 3:                g2.setColor(Color.RED);
                    break;
                case 4:                g2.setColor(Color.WHITE);
                    break;
                case 5:                g2.setColor(Color.GREEN);
                    break;
            }
            g2.drawImage((aPowerUp.getFilepath()), aPowerUp.getX(), aPowerUp.getY(), null);
            aPowerUp.powerUpMove();
            if (aPowerUp.createRect().intersects(Player.rect())) {
	            aPowerUp.givePower();
	            getPowerUp().remove(aPowerUp);
                break;
            }
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        /** Draws and handles nyan cat special character*/
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        if (isSpecialFlag() || Math.random() > 0.999) {
            if (!isSpecialFlag()) {
                setRem_soundtrack(Main.getSoundtrack());
                setNyn_x(0);
            }
            if (!isNyn_flag()) {
                Main.setSoundtrack(5);
                int nyany = 10;
                for (int i = 0; i < 15; i++) {
                    if (i < 3) {
                        g2.setColor(new Color(255, 0, 0));
                    } else if (i < 6) {
                        g2.setColor(new Color(255, 255, 0));
                    } else if (i < 9) {
                        g2.setColor(new Color(0, 255, 0));
                    } else if (i < 12) {
                        g2.setColor(new Color(0, 255, 255));
                    } else if (i < 15) {
                        g2.setColor(new Color(0, 0, 255));
                    }
                    g2.fillRect(getNyn_x() - 180, nyany + 2 * i, 200, 2);
                }
                g2.drawImage(getNyan_cat(), getNyn_x(), nyany, this);
            }
            setSpecialFlag(true);
            setNyn_x(getNyn_x() + 1);
            if (getNyn_x() >= Screen.width || isNyn_flag()) {
                Main.setSoundtrack(getRem_soundtrack());
                setNyn_flag(false);
                setSpecialFlag(false);
                setNyn_x(0);
            }
        }
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        /** Draws and handles tutorial*/
        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
        if (isTut1()) {
            g2.setColor(Color.ORANGE);
            g2.drawString("Press A to move left and D to move right", 10, (int) (Screen.height * 0.9));
        }
        if (isTut2()) {
            g2.setColor(Color.ORANGE);
            g2.drawString("Press S to Shoot and start the game", 10, (int) (Screen.height * 0.9));
        }
        if (isTut3()) {
            g2.setColor(Color.ORANGE);
            g2.drawString("Shoot all the enemies to win. Some will take more than one shot", 10, (int) (Screen.height * 0.6));
        }
        if (isTut4()) {
            g2.setColor(Color.ORANGE);
            g2.drawString("Avoid enemy bullets or you will loose health", 10, (int) (Screen.height * 0.6));
            g2.drawString("If you run out of health you will loose a life", 10, (int) (Screen.height * 0.6 + 20));
        }
        if (isTut5()) {
            g2.setColor(Color.ORANGE);
            g2.drawString("Grab falling powerups to upgrade your player", 10, (int) (Screen.height * 0.6));
            g2.drawString("Shoot enemies to improve your score so you can buy upgrades.", 10, (int) (Screen.height * 0.6 + 20));
        }
        if (isTut6()) {
            g2.setColor(Color.ORANGE);
            g2.drawString("Shoot the nyan cat for bonus points!!!", 200, 40);
        }
        g2.setColor(Color.ORANGE);
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /** Handles Shield timer*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

    private class ShieldTimer implements Runnable {

        @Override
        public void run() {
            while (true) {
                if (GameScreen.getShieldActive() == 0) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    GameScreen.setShieldActive(Player.getShield());
                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
/**Handles Tutorial outputs timer*/
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

class Tutorial implements Runnable {

    @Override
    public void run() {
        GameScreen.setTut1(true);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GameScreen.setTut1(false);
        GameScreen.setTut2(true);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GameScreen.setTut2(false);
        GameScreen.setTut3(true);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GameScreen.setTut3(false);
        GameScreen.setTut4(true);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GameScreen.setTut4(false);
        GameScreen.setTut5(true);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GameScreen.setTut5(false);
        GameScreen.setTut6(true);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GameScreen.setTut6(false);
    }

}

package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**      _____    _ _____   _____ _____            _____  ______    _____  _____ _____  ______ ______ _   _
 *      / / / |  | |  __ \ / ____|  __ \     /\   |  __ \|  ____|  / ____|/ ____|  __ \|  ____|  ____| \ | |
 *     / / /| |  | | |__) | |  __| |__) |   /  \  | |  | | |__    | (___ | |    | |__) | |__  | |__  |  \| |
 *    / / / | |  | |  ___/| | |_ |  _  /   / /\ \ | |  | |  __|    \___ \| |    |  _  /|  __| |  __| | . ` |
 *   / / /  | |__| | |    | |__| | | \ \  / ____ \| |__| | |____   ____) | |____| | \ \| |____| |____| |\  |
 *  /_/_/    \____/|_|     \_____|_|  \_\/_/    \_\_____/|______| |_____/ \_____|_|  \_\______|______|_| \_|
 *
 */

class UpgradeScreen extends JPanel {
    //--------------------------------------------------------------------------------------------------//
    /**Cost of different upgrades stored in the cost variables*/
    //--------------------------------------------------------------------------------------------------//
    private static          int                 SpeedCost           = 1;
    private static          int                 RateCost            = 1;
    private static          int                 ShieldCost          = 1;
    //--------------------------------------------------------------------------------------------------//
    /**Set this true if score hits 0. Prevents player from getting upgrades after score runs out*/
    //--------------------------------------------------------------------------------------------------//
    private static          boolean             outOfPoints         = false;
    //--------------------------------------------------------------------------------------------------//
    /**Definitions of all of the buttons*/
    /**Imported images have been resized*/
    //--------------------------------------------------------------------------------------------------//
    private         final   JButton             addFireRate         = new JButton(new ImageIcon(new ImageIcon("images\\bullet_icon.png").getImage().getScaledInstance((int) (Screen.width * 0.12625), (int) (Screen.height * 0.0467), Image.SCALE_SMOOTH)));
    private         final   JButton             addSpeed            = new JButton(new ImageIcon(new ImageIcon("images\\speed_icon.png").getImage().getScaledInstance((int) (Screen.width * 0.12625), (int) (Screen.height * 0.0467), Image.SCALE_SMOOTH)));
    public          final   JButton             play                = new JButton(new ImageIcon(new ImageIcon("images\\continue.png").getImage().getScaledInstance((int) (Screen.width * 0.3125), (int) (Screen.height * 0.133), Image.SCALE_SMOOTH)));
    public          final   JButton             exit                = new JButton(new ImageIcon(new ImageIcon("images\\exitButton.png").getImage().getScaledInstance((int) (Screen.width * 0.3125), (int) (Screen.height * 0.133), Image.SCALE_SMOOTH)));
    private         final   JButton             shield              = new JButton(new ImageIcon(new ImageIcon("images\\ShieldButton.png").getImage().getScaledInstance((int) (Screen.width * 0.12625), (int) (Screen.height * 0.0467), Image.SCALE_SMOOTH)));
    //--------------------------------------------------------------------------------------------------//
    /**Stores the strings for the weapon images*/
    //--------------------------------------------------------------------------------------------------//
    private         final   String[]            weaponImages        = {"standard.png", "heavy.png", "spread3.png", "spread5.png", "spread7.png", "laser.png"};
    //--------------------------------------------------------------------------------------------------//
    /**Holds the string for the weapon image to show on the upgrade screen*/
    //--------------------------------------------------------------------------------------------------//
    private                 String              weapon              = "images\\" + weaponImages[0];
    //--------------------------------------------------------------------------------------------------//
    /**Image placeholders to hold weapon image and background image*/
    //--------------------------------------------------------------------------------------------------//
    private                 Image               weaponImage;
    private                 Image               backgroundImage;
    //--------------------------------------------------------------------------------------------------//
    /**Instantiates labels for item costs in the upgrade screen and indicators*/
    //--------------------------------------------------------------------------------------------------//
    private final JLabel              label               = new JLabel("<html><h1>Weapon: </h1></html>");
    private         final   JLabel              outOfPointsLabel    = new JLabel("            ");
    private         final   JLabel              fireRateCost        = new JLabel("Cost: " + RateCost);
    private         final   JLabel              speedCost           = new JLabel("Cost: " + SpeedCost);
    private         final   JLabel              shieldCost          = new JLabel("Cost: " + ShieldCost);
    //--------------------------------------------------------------------------------------------------//
    /**Counter to flash the out of points for a set amount of time*/
    //--------------------------------------------------------------------------------------------------//
    private                 int                 outOfPointsTimer    = 0;
    //--------------------------------------------------------------------------------------------------//
    /**Holds the alignment properties for the screen*/
    //--------------------------------------------------------------------------------------------------//
    private static  final   GridBagConstraints  c                   = new GridBagConstraints();
    //--------------------------------------------------------------------------------------------------//
    /*****************************************END DECLARATIONS******************************************/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Constructor initialises all buttons and sets their locations. Also sets screen layout and shows background image*/
    /**Adds combo boxes*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    UpgradeScreen() throws IOException {
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise combo box options and store as a string*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        String[] comboBoxItems = {"1.Standard - 0pts", "2.Heavy - 500pts", "3.Spread-3 - 1000pts", "4.Spread-5 - 10000pts", "5.Spread-7 - 20000pts", "6.Laser - 100000pts"};
        JComboBox<String> cb = new JComboBox<String>(comboBoxItems);
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*
         *Item listener checks for selected weapon type and
         *shows weapon image based on this.
         */
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        cb.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                String str = "0";

                if (e.getStateChange() == ItemEvent.SELECTED) {
                    str = e.getItem().toString();
                }

                switch (Integer.parseInt(str.substring(0, 1))) {
                    case 1:
                        Player.setWeapon_type(0);
                        outOfPoints = false;
                        break;
                    case 2:
                        if (Player.getScore() >= 500) {
                            Player.setScore(Player.getScore() - 500);
                            Player.setWeapon_type(1);
                        } else {
                            outOfPoints = true;
                        }
                        break;
                    case 3:
                        if (Player.getScore() >= 1000) {
                            Player.setScore(Player.getScore() - 1000);
                            Player.setWeapon_type(2);
                        } else {
                            outOfPoints = true;
                        }
                        break;
                    case 4:
                        if (Player.getScore() >= 10000) {
                            Player.setScore(Player.getScore() - 10000);
                            Player.setWeapon_type(3);
                        } else {
                            outOfPoints = true;
                        }
                        break;
                    case 5:
                        if (Player.getScore() >= 20000) {
                            Player.setScore(Player.getScore() - 20000);
                            Player.setWeapon_type(4);
                        } else {
                            outOfPoints = true;
                        }
                        break;
                    case 6:
                        if (Player.getScore() >= 100000) {
                            Player.setScore(Player.getScore() - 100000);
                            Player.setWeapon_type(5);
                        } else {
                            outOfPoints = true;
                        }
                        break;
                }
                weapon = "images\\" + weaponImages[Player.getWeapon_type()];
            }
        });
        /**
         * For each button: Set background clear, set border
         * to not show up and add the mouse listener for
         * hover colour
         */
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise play button*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        play.setBackground(new Color(0, 0, 0, 0));
        play.setBorderPainted(false);
        play.addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise exit button*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        exit.setBackground(new Color(0, 0, 0, 0));
        exit.setBorderPainted(false);
        exit.addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise add speed button*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        addSpeed.setBackground(new Color(0, 0, 0, 0));
        addSpeed.setBorderPainted(false);
        addSpeed.addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise fire rate button*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        addFireRate.setBackground(new Color(0, 0, 0, 0));
        addFireRate.setBorderPainted(false);
        addFireRate.addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise shield button*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        shield.setBackground(new Color(0, 0, 0, 0));
        shield.setBorderPainted(false);
        shield.addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise save button*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        JButton save = new JButton(new ImageIcon(new ImageIcon("images\\save.png").getImage().getScaledInstance((int) (Screen.width * 0.3125), (int) (Screen.height * 0.17), Image.SCALE_SMOOTH)));
        save.setBackground(new Color(0, 0, 0, 0));
        save.setBorderPainted(false);
        save.addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Set background image*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        String background = "images\\UpScreen.png";
        backgroundImage = ImageIO.read(new File(background));
        backgroundImage = backgroundImage.getScaledInstance(Screen.width, (int) (Screen.height * 0.97), Image.SCALE_SMOOTH);
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Define Layout for this panel*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        this.setLayout(new GridBagLayout());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Add the title*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        JLabel label2 = new JLabel();
        this.add(label2);
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Set the color of the labels to yellow*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        fireRateCost.setForeground(Color.YELLOW);
        speedCost.setForeground(Color.YELLOW);
        shieldCost.setForeground(Color.YELLOW);
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Set the initial color of the out of points labels to yellow*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        outOfPointsLabel.setForeground(Color.BLACK);
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Add the buttons in the correct places*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        c.insets = new Insets(0, Screen.width / 10, 0, 0);
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.CENTER;
        this.add(play, c);
        c.gridx = 1;
        this.add(exit, c);
        c.gridx = 0;
        c.gridy = 1;
        this.add(addSpeed, c);
        c.gridx = 1;
        this.add(speedCost, c);
        c.gridx = 0;
        c.gridy = 2;
        this.add(addFireRate, c);
        c.gridx = 1;
        this.add(fireRateCost, c);
        c.gridy = 3;
        c.gridx = 0;
        this.add(shield, c);
        c.gridx = 1;
        this.add(shieldCost, c);
        c.gridy = 4;
        c.gridx = 0;
        c.fill = GridBagConstraints.CENTER;
        label.setForeground(Color.YELLOW);
        this.add(label, c);
        c.insets = new Insets(0, (int) (Screen.width * 0.125), 0, (int) (Screen.width * 0.125));
        c.gridx = 1;
        this.add(cb, c);
        c.gridy = 5;
        c.anchor = GridBagConstraints.PAGE_END;
        c.gridx = 0;
        c.gridy = 6;
        this.add(outOfPointsLabel, c);
        c.gridy = 5;
        c.gridx = 1;
        this.add(save, c);
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Add the listeners to the buttons*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        SpeedListener speedListener = new SpeedListener();
        this.addSpeed.addActionListener(speedListener);
        RateListener rateListener = new RateListener();
        this.addFireRate.addActionListener(rateListener);
        ShieldListener shieldListener = new ShieldListener();
        this.shield.addActionListener(shieldListener);
        save.addActionListener(new SaveListener());
    }

    //++++++++++++++++++++++++++++++++++++++++++++++++++//
    /*This function places the upgrade values on the screen.*/
    //++++++++++++++++++++++++++++++++++++++++++++++++++//
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        try {
            weaponImage = ImageIO.read(new File(weapon));
        } catch (IOException e) {
            e.printStackTrace();
        }
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Set the background color to green*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        this.setBackground(Color.BLACK);
        g2.drawImage(backgroundImage, 0, 0, null);
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*
        *Draw the score, as well as the speed cost, rate,
        *and fire rate, cost, rate.
        */
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        g2.setFont(new Font("Standard", Font.BOLD, 20));
        g2.setColor(Color.yellow);
        g2.drawImage(weaponImage, label.getX() + (int) (Screen.width * 0.18), label.getY() + (int) (Screen.height * 0.02), null);
        g2.drawString(String.format("Score: %d", Player.getScore()), (int) (0.6 * Screen.width), (int) (0.8 * Screen.height));
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*When out of points display the out of points label*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        if (outOfPoints) {
            g2.setColor(Color.yellow);
            outOfPointsLabel.setForeground(Color.YELLOW);
            outOfPointsLabel.setText("Out of Points");
            if (outOfPointsTimer < 100) {
                outOfPointsTimer++;
            } else if (outOfPointsTimer >= 100) {
                outOfPointsLabel.setForeground(Color.BLACK);
                outOfPointsLabel.setText("           ");
                outOfPoints = false;
                outOfPointsTimer = 0;
            }
        }
        fireRateCost.setText("Cost: " + RateCost);
        speedCost.setText("Cost: " + SpeedCost);
        shieldCost.setText("Cost: " + ShieldCost);
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*
        * This loop creates a rainbow bar showing
        * the players speed level.
        */
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        for (int j = 0; j < Player.getSpeed() * 10; j++) {
            if (j * 5 < 255) {
                g2.setColor(new Color(255, j * 5, 0));
            } else if (j * 5 < (255 * 2)) {
                g2.setColor(new Color(255 - ((j * 5) - 255), 255, 0));
            } else if (j * 2 < 255) {
                g2.setColor(new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * (255 - j))));
            } else {
                g2.setColor(new Color(0, 0, 0, 0));
            }
            g2.fillRect((j) + (Screen.width / 3), addSpeed.getY() + addSpeed.getHeight() / 2, Screen.height / 120, Screen.height / 60);
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*
        *This loop creates a rainbow bar showing the
        *players fire rate level.
        */
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        for (int j = 0; j < Player.getFireRate() * 10; j++) {
            if (j * 5 < 255) {
                g2.setColor(new Color(255, j * 5, 0));
            } else if (j * 5 < (255 * 2)) {
                g2.setColor(new Color(255 - ((j * 5) - 255), 255, 0));
            } else if (j < 255) {
                g2.setColor(new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * (255 - j))));
            } else {
                g2.setColor(new Color(0, 0, 0, 0));
            }
            g2.fillRect((j) + (Screen.width / 3), addFireRate.getY() + addFireRate.getHeight() / 2, Screen.height / 120, Screen.height / 60);
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*
        * This loop creates a rainbow bar showing the players
        * shield level.
        */
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        for (int j = 0; j < Player.getShield() * 10; j++) {
            if (j * 5 < 255) {
                g2.setColor(new Color(0, j * 5, 255));
            } else if (j * 5 < (255 * 2)) {
                g2.setColor(new Color(0, 255, 255 - ((j * 5) - 255)));
            } else if (j < 255) {
                g2.setColor(new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * (255 - j))));
            } else {
                g2.setColor(new Color(0, 0, 0, 0));
            }
            g2.fillRect((j) + (Screen.width / 3), shield.getY() + shield.getHeight() / 2, Screen.height / 120, Screen.height / 60);
        }
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /** Add a listener to check for speed button clicks*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static class SpeedListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (Player.getScore() >= UpgradeScreen.SpeedCost) {
                Player.setUpgradeSpeed(Player.getUpgradeSpeed() + 0.1);
                Player.setScore(Player.getScore() - UpgradeScreen.SpeedCost);
                UpgradeScreen.SpeedCost *= 2;
                outOfPoints = false;
            } else {
                outOfPoints = true;
            }
        }
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Add a listener to check for bullet fire rate*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static class RateListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (Player.getScore() >= UpgradeScreen.RateCost) {
                Player.setUpgradeFireRate(Player.getUpgradeFireRate() + 0.1);
                Player.setScore(Player.getScore() - UpgradeScreen.RateCost);
                UpgradeScreen.RateCost *= 2;
                outOfPoints = false;
            } else {
                outOfPoints = true;
            }
        }
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Add a listener to check for a change in shield*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static class ShieldListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (Player.getScore() >= UpgradeScreen.ShieldCost) {
                Player.setShield(Player.getShield() + 1);
                Player.setScore(Player.getScore() - UpgradeScreen.ShieldCost);
                UpgradeScreen.ShieldCost *= 2;
                outOfPoints = false;
            } else {
                outOfPoints = true;
            }
            GameScreen.setShieldActive(Player.getShield());
        }
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Check to see if the game should be saved*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static class SaveListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                GameScreen.saveState();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
        }
    }
}


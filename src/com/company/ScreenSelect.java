package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**      _________  _____ _____  ______ ______ _   _    _____ ______ _      ______ _____ _______
 *      / / / ____|/ ____|  __ \|  ____|  ____| \ | |  / ____|  ____| |    |  ____/ ____|__   __|
 *     / / / (___ | |    | |__) | |__  | |__  |  \| | | (___ | |__  | |    | |__ | |       | |
 *    / / / \___ \| |    |  _  /|  __| |  __| | . ` |  \___ \|  __| | |    |  __|| |       | |
 *   / / /  ____) | |____| | \ \| |____| |____| |\  |  ____) | |____| |____| |___| |____   | |
 *  /_/_/  |_____/ \_____|_|  \_\______|______|_| \_| |_____/|______|______|______\_____|  |_|
 *
 */

class ScreenSelect extends JFrame {
    //-------------------------------------------------------------------------------------------//
    /** Set whether the screen should be visible or not*/
    //-------------------------------------------------------------------------------------------//
    private static boolean scrVis = true;
    //-------------------------------------------------------------------------------------------//
    /** Strings to be placed into the combo box*/
    //-------------------------------------------------------------------------------------------//
    private final String[] comboBoxItems = {"800x600", "600x800"};
    //-------------------------------------------------------------------------------------------//
    /** Adds the strings to the combo box*/
    //-------------------------------------------------------------------------------------------//
    private final JComboBox<String> cb = new JComboBox<String>(comboBoxItems);
    //-------------------------------------------------------------------------------------------//
    /**Creates the panel for selection option*/
    //-------------------------------------------------------------------------------------------//
    private final JPanel panel = new JPanel();
    //-------------------------------------------------------------------------------------------//
    /**Creates a label to prompt user to select screen size*/
    //-------------------------------------------------------------------------------------------//
    private final JLabel label   = new JLabel();
    //-------------------------------------------------------------------------------------------//
    /**Creates a label to prompt user to select screen size*/
    //-------------------------------------------------------------------------------------------//
    private final JButton button = new JButton(new ImageIcon("images\\DOGEE.png"));
    //--------------------------------------------------------------------------------------------------//
    /*****************************************END DECLARATIONS******************************************/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Check to notify whether screen should still be visible or not*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void checkScreenStatus() {
        if (!scrVis) {
            this.setVisible(false);
        }
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Initialises the panel and ensures that all parts are in the correct place*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void init() {
        button.setBackground(new Color(0, 0, 0, 0));
        button.setBorderPainted(false);
        this.setName("Screen Size Selection Panel");
        Dimension SystemScreen = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(SystemScreen.width / 2 - 300, SystemScreen.height / 2 - 400);
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        this.setSize(400, 100);
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*This listener checks to see if the state has changed*/
        /*Modifies screen width and height*/
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++//
        cb.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                String str = e.getItem().toString();

                if (str.substring(0, 1).equals("8")) {
                    Screen.width = 800;
                    Screen.height = 600;

                }
                if (str.substring(0, 1).equals("6")) {
                    Screen.width = 600;
                    Screen.height = 800;
                }
            }
        });
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Confirms screen selection options and activates screen*/
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++//
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.getFlag()[2] = false;
                scrVis = false;
            }
        });
        panel.add(cb);
        label.setText("Select Desired Screen Size");
        //++++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Add all the components to the screen*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++++//
        c.gridx = 0;
        c.gridy = 0;
        this.add(label,c);
        c.gridx = 1;
        this.add(panel,c);
        c.gridx = 2;
        this.add(button,c);
        this.setVisible(true);
        //++++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Cause the screen to terminate program if closed*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++++//
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

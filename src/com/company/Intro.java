package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 *
 *        _________ _   _ _______ _____   ____
 *       / / /_   _| \ | |__   __|  __ \ / __ \
 *      / / /  | | |  \| |  | |  | |__) | |  | |
 *     / / /   | | | . ` |  | |  |  _  /| |  | |
 *    / / /   _| |_| |\  |  | |  | | \ \| |__| |
 *   /_/_/   |_____|_| \_|  |_|  |_|  \_\\____/
 * This class handles the splash screen
 */

class Intro extends JWindow {

    //---------------------------------------------------------------------------------------//
    /**Strings to hold splash screen sequence file paths*/
    //---------------------------------------------------------------------------------------//
    private static final String S1 = "images\\S1.png";
    private static final String S2 = "images\\S2.png";
    private static final String S3 = "images\\S3.png";
    private static final String S4 = "images\\S4.png";
    private static final String S5 = "images\\S5.png";
    private static final String S6 = "images\\S6.png";
    private static final String S7 = "images\\S7.png";
    private static final String S8 = "images\\S8.png";
    private static final String S9 = "images\\S9.png";
    private static final String S10 = "images\\S10.png";
    private static final String S11 = "images\\S11.png";
    //---------------------------------------------------------------------------------------//
    /**String to hold the memory value from memory test*/
    //---------------------------------------------------------------------------------------//
    private static String memory;
    //---------------------------------------------------------------------------------------//
    /**Buffered image placeholders*/
    //---------------------------------------------------------------------------------------//
    private static BufferedImage P1;
    private static BufferedImage P2;
    private static BufferedImage P3;
    private static BufferedImage P4;
    private static BufferedImage P5;
    private static BufferedImage P6;
    private static BufferedImage P7;
    private static BufferedImage P8;
    private static BufferedImage P9;
    private static BufferedImage P10;
    private static BufferedImage P11;
    //---------------------------------------------------------------------------------------//
    /**Integer i loops and sets what images to show. Causes an animation effect.*/
    //---------------------------------------------------------------------------------------//
    private int i = 0;
    //---------------------------------------------------------------------------------------//
    /**Set and initialise JPanel*/
    //---------------------------------------------------------------------------------------//
    private final JPanel panel = new JPanel() {
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initial paint stuff*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        public void paint(Graphics g) {
            super.paint(g);
            Graphics2D g2 = (Graphics2D) g;
            //++++++++++++++++++++++++++++++++++++++++++++++++++//
            /*Set Background color*/
            //++++++++++++++++++++++++++++++++++++++++++++++++++//
            this.setBackground(new Color(0, 0, 0, 0));
            //++++++++++++++++++++++++++++++++++++++++++++++++++//
            /*Case statement runs through images to cause
            * animation effect
            */
            //++++++++++++++++++++++++++++++++++++++++++++++++++//
            switch (getI()) {
                case 0:
                    g2.drawImage(getP1(), 0, 0, null);
                    setI(getI() + 1);
                    break;
                case 1:
                    g2.drawImage(getP2(), 0, 0, null);
                    setI(getI() + 1);
                    break;
                case 2:
                    g2.drawImage(getP3(), 0, 0, null);
                    setI(getI() + 1);
                    break;
                case 3:
                    g2.drawImage(getP4(), 0, 0, null);
                    setI(getI() + 1);
                    break;
                case 4:
                    g2.drawImage(getP5(), 0, 0, null);
                    setI(getI() + 1);
                    break;
                case 5:
                    g2.drawImage(getP6(), 0, 0, null);
                    setI(getI() + 1);
                    break;
                case 6:
                    g2.drawImage(getP7(), 0, 0, null);
                    setI(getI() + 1);
                    break;
                case 7:
                    g2.drawImage(getP8(), 0, 0, null);
                    setI(getI() + 1);
                    break;
                case 8:
                    g2.drawImage(getP9(), 0, 0, null);
                    setI(getI() + 1);
                    break;
                case 9:
                    g2.drawImage(getP10(), 0, 0, null);
                    setI(getI() + 1);
                    break;
                case 10:
                    g2.drawImage(getP11(), 0, 0, null);
                    break;
            }
            //++++++++++++++++++++++++++++++++++++++++++++++++++//
            /*Set Color to black*/
            //++++++++++++++++++++++++++++++++++++++++++++++++++//
            g2.setColor(Color.BLACK);
            //++++++++++++++++++++++++++++++++++++++++++++++++++//
            /*Draw the memory from memory test*/
            //++++++++++++++++++++++++++++++++++++++++++++++++++//
            g2.drawString("Memory:       " + getMemory(), 135, 455);
        }
    };
    //---------------------------------------------------------------------------------------//
    /*****************************************END DECLARATIONS*******************************/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Getters for filepath strings*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static String getS1() {
        return S1;
    }
    private static String getS2() {
        return S2;
    }
    private static String getS3() {
        return S3;
    }
    private static String getS4() {
        return S4;
    }
    private static String getS5() {
        return S5;
    }
    private static String getS6() {
        return S6;
    }
    private static String getS7() {
        return S7;
    }
    private static String getS8() {
        return S8;
    }
    private static String getS9() {
        return S9;
    }
    private static String getS10() {
        return S10;
    }
    private static String getS11() {
        return S11;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Getters and setters for memory*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static String getMemory() {
        return memory;
    }
    public static void setMemory(String memory) {
        Intro.memory = memory;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Getters and setters for the buffered images*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static BufferedImage getP1() {
        return P1;
    }
    private static void setP1(BufferedImage p1) {
        P1 = p1;
    }
    private static BufferedImage getP2() {
        return P2;
    }
    private static void setP2(BufferedImage p2) {
        P2 = p2;
    }
    private static BufferedImage getP3() {
        return P3;
    }
    private static void setP3(BufferedImage p3) {
        P3 = p3;
    }
    private static BufferedImage getP4() {
        return P4;
    }
    private static void setP4(BufferedImage p4) {
        P4 = p4;
    }
    private static BufferedImage getP5() {
        return P5;
    }
    private static void setP5(BufferedImage p5) {
        P5 = p5;
    }
    private static BufferedImage getP6() {
        return P6;
    }
    private static void setP6(BufferedImage p6) {
        P6 = p6;
    }
    private static BufferedImage getP7() {
        return P7;
    }
    private static void setP7(BufferedImage p7) {
        P7 = p7;
    }
    private static BufferedImage getP8() {
        return P8;
    }
    private static void setP8(BufferedImage p8) {
        P8 = p8;
    }
    private static BufferedImage getP9() {
        return P9;
    }
    private static void setP9(BufferedImage p9) {
        P9 = p9;
    }
    private static BufferedImage getP10() {
        return P10;
    }
    private static void setP10(BufferedImage p10) {
        P10 = p10;
    }
    private static BufferedImage getP11() {
        return P11;
    }
    private static void setP11(BufferedImage p11) {
        P11 = p11;
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Initialisation is simply loading images and centering screen*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void init() throws IOException {
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Center Screen*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Load Images*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        this.setLocation(dim.width / 2 - 300, dim.height / 2 - 300);
        setP1(ImageIO.read(new File(getS1())));
        setP2(ImageIO.read(new File(getS2())));
        setP3(ImageIO.read(new File(getS3())));
        setP4(ImageIO.read(new File(getS4())));
        setP5(ImageIO.read(new File(getS5())));
        setP6(ImageIO.read(new File(getS6())));
        setP7(ImageIO.read(new File(getS7())));
        setP8(ImageIO.read(new File(getS8())));
        setP9(ImageIO.read(new File(getS9())));
        setP10(ImageIO.read(new File(getS10())));
        setP11(ImageIO.read(new File(getS11())));
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Add the panel to the JWindow*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        this.add(getPanel());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Set the background to transparent*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        this.setBackground(new Color(0, 0, 0, 0));
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Getter for integer i which determines the animation sequence*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private int getI() {
        return i;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Setter for integer i which determines the animation sequence*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private void setI(int i) {
        this.i = i;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Getter for this screens JPanel*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private JPanel getPanel() {
        return panel;
    }
}



package com.company;

import javax.sound.sampled.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *      _________  _
 *     / / /  __ \| |
 *    / / /| |__) | | __ _ _   _  ___ _ __
 *   / / / |  ___/| |/ _` | | | |/ _ \ '__|
 *  / / /  | |    | | (_| | |_| |  __/ |
 * /_/_/   |_|    |_|\__,_|\__, |\___|_|
 *                          __/ |
 *                         |___/
 *
 * Player class creates player that is then controlled by the user.
 * */

class Player {

	//-------------------------------------------------------------------------------------------//
	/**Initialise y position and size of player*/
	//-------------------------------------------------------------------------------------------//
    private static final int y = (int) (0.8125 * Screen.height);
    private static final int width = Screen.width/30;
	private static final int height = Screen.height/20;
	//-------------------------------------------------------------------------------------------//
	/**Create array to hold all bullets currently on screen*/
	//-------------------------------------------------------------------------------------------//
    private static final ArrayList<Bullet> activeBullets = new ArrayList<Bullet>(0);
	private static final int bulletCount = 0;
	//-------------------------------------------------------------------------------------------//
	/**Set player speed*/
	//-------------------------------------------------------------------------------------------//
    private static final double baseSpeed = 0.5;
	private static double powerUpSpeed = 0.5;
	private static double upgradeSpeed = 0.0;
	private static double speed = getBaseSpeed() + getUpgradeSpeed() + getPowerUpSpeed();
	//-------------------------------------------------------------------------------------------//
	/**Set player fireRate*/
	//-------------------------------------------------------------------------------------------//
	private static final double baseFireRate = 1;
	private static double powerUpFireRate = 0.0;
	private static double upgradeFireRate = 0.0;
	private static double fireRate = getBaseFireRate() + getPowerUpFireRate() + getUpgradeFireRate();
	//-------------------------------------------------------------------------------------------//
	/**set all other player stats*/
	//-------------------------------------------------------------------------------------------//
    private static int lives = 3;
    private static int health = 100;
    private static int shield = 1;
    private static int score = 0;
    private static int x = 0;
    private static boolean invincible = false;
    private static int weapon_type = 0;
    private static boolean turnLeft;
    private static boolean turnRight;
	private static final Font playerFont = new Font("default", Font.BOLD, 16);
	//-------------------------------------------------------------------------------------------//
	/**set shot timer that restricts rate of fire*/
	//-------------------------------------------------------------------------------------------//
    private static boolean shotTimerCountdown = false;
    private static double shotTimer = 0.0;
	//-------------------------------------------------------------------------------------------//
	/**Initialise variables to make gunshot work*/
	//-------------------------------------------------------------------------------------------//
	private static File gunshot;
	private static AudioInputStream audio;
	private static AudioFormat format;
	private static DataLine.Info info;
	private static Clip sound;

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Get and Set method*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	public static int getY() { return y; }
	public static int getWidth() { return width; }
	public static ArrayList<Bullet> getActiveBullets() { return activeBullets; }
	public static Font getPlayerFont() { return playerFont; }
	private static double getBaseSpeed() { return baseSpeed; }
	private static double getBaseFireRate() { return baseFireRate; }
	public static int getHeight() { return height; }
	private static int getBulletCount() { return bulletCount; }
	public static int getLives() { return lives; }
	public static void setLives(int lives) { Player.lives = lives; }
	public static int getShield() { return shield; }
	public static void setShield(int shield) { Player.shield = shield; }
	private static double getPowerUpSpeed() { return powerUpSpeed; }
	public static void setPowerUpSpeed(double powerUpSpeed) { Player.powerUpSpeed = powerUpSpeed; }
	public static double getUpgradeSpeed() { return upgradeSpeed; }
	public static void setUpgradeSpeed(double upgradeSpeed) { Player.upgradeSpeed = upgradeSpeed; }
	public static double getSpeed() { return speed; }
	public static void setSpeed(double speed) { Player.speed = speed; }
	private static double getPowerUpFireRate() { return powerUpFireRate; }
	public static void setPowerUpFireRate(double powerUpFireRate) { Player.powerUpFireRate = powerUpFireRate; }
	public static double getUpgradeFireRate() { return upgradeFireRate;}
	public static void setUpgradeFireRate(double upgradeFireRate) { Player.upgradeFireRate = upgradeFireRate; }
	public static double getFireRate() { return fireRate;    }
	public static void setFireRate(double fireRate) { Player.fireRate = fireRate; }
	public static int getScore() { return score; }
	public static void setScore(int score) { Player.score = score; }
	public static int getX() { return x; }
	public static void setX(int x) { Player.x = x; }
	public static boolean isInvincible() { return invincible; }
	public static void setInvincible(boolean invincible) { Player.invincible = invincible; }
	public static int getWeapon_type() { return weapon_type; }
	public static void setWeapon_type(int weapon_type) { Player.weapon_type = weapon_type; }
	public static boolean isTurnLeft() { return turnLeft; }
	public static void setTurnLeft(boolean turnLeft) { Player.turnLeft = turnLeft; }
	public static boolean isTurnRight() { return turnRight; }
	public static void setTurnRight(boolean turnRight) { Player.turnRight = turnRight; }
	private static boolean isShotTimerCountdown() { return shotTimerCountdown; }
	private static void setShotTimerCountdown(boolean shotTimerCountdown) { Player.shotTimerCountdown = shotTimerCountdown; }
	private static double getShotTimer() { return shotTimer; }
	private static void setShotTimer(double shotTimer) { Player.shotTimer = shotTimer; }
	private static File getGunshot() { return gunshot; }
	private static void setGunshot(File gunshot) { Player.gunshot = gunshot; }
	private static AudioInputStream getAudio() { return audio; }
	private static void setAudio(AudioInputStream audio) { Player.audio = audio; }
	private static AudioFormat getFormat() { return format; }
	private static void setFormat(AudioFormat format) { Player.format = format; }
	private static DataLine.Info getInfo() { return info; }
	private static void setInfo(DataLine.Info info) { Player.info = info; }
	public static Clip getSound() { return sound; }
	private static void setSound(Clip sound) { Player.sound = sound; }
	public static int getHealth() { return health; }
	public static void setHealth(int health) {
		Player.health = health;
	}

	public static void decreaseHealth(int lostHealth) {
        setHealth(getHealth() - lostHealth);
    }
    public static void heal() {
        setHealth(getHealth() + 100);
    }
    public static void reinitialiseStats() {
        setFireRate(getBaseFireRate() + getUpgradeFireRate() + getPowerUpFireRate());
        setSpeed(getBaseSpeed() + getUpgradeSpeed() + getPowerUpSpeed());
    }
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Set players rectangle to be used for collision detection*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public static Rectangle rect() {
        return new Rectangle(getX(), getY(), getWidth(), getHeight());
    }
    public static void moveRight() {
        if (getX() < Screen.width - Player.rect().width *2) {
            setX(getX() + (1 + (int) getSpeed()));
        }
    }
    public static void moveLeft() {
        if (getX() > 0) {
            setX(getX() - (1 + (int) getSpeed()));
        }
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Setup gun shot sound*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public static void init() {
        setGunshot(new File("Sounds\\gunshot.wav"));
        try {
            setAudio(AudioSystem.getAudioInputStream(getGunshot()));
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setFormat(getAudio().getFormat());
        setInfo(new DataLine.Info(Clip.class, getFormat()));
        try {
            setSound((Clip) AudioSystem.getLine(getInfo()));
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        try {
            getSound().open(getAudio());
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        getSound().loop(Clip.LOOP_CONTINUOUSLY);
        getSound().stop();
    }

    //creates bullet and adds bullet to arraylist
    public static void shoot() {
        //check shot has not been fired too recently
        if (!isShotTimerCountdown()) {
            getSound().setFramePosition(0);
            //import and play gunshot audio file
            //start countdown to limit fire-rate
            setShotTimerCountdown(true);
            //create bullet
            Bullet shot;
	        //Select which weapon is being used
            if (getWeapon_type() == 0) {
                shot = new Standard_Bullet(1);
            } else if (getWeapon_type() == 1) {
                shot = new Heavy_Bullet();
            } else if (getWeapon_type() == 2) {
                shot = new Spread_Bullet(0);
                shot.setNumber(3);
            } else if (getWeapon_type() == 3) {
                shot = new Spread_Bullet(0);
                shot.setNumber(5);
            } else if (getWeapon_type() == 4) {
                shot = new Spread_Bullet(0);
                shot.setNumber(7);
            } else if (getWeapon_type() == 5) {
                shot = new Lazer_Bullet();
            } else {
                shot = new Standard_Bullet(1);
            }
            shot.setXPos(getX());
            getActiveBullets().add(getBulletCount(), shot);
	        //Creating spreading shot for spread weapons
            for (int i = 1; i < shot.getNumber(); i++) {
                Spread_Bullet shot1 = new Spread_Bullet(i);
	            shot1.setXPos(Player.getX());
                getActiveBullets().add(getBulletCount(), shot1);
            }
        }
    }

    //creates delay for firerate
    public static void countDown() {
        if (isShotTimerCountdown()) {
            if (getShotTimer() >= (100 / getFireRate())) {
                setShotTimerCountdown(false);
                setShotTimer(0);
            } else {
                setShotTimer(getShotTimer() + 1);
            }
        }
    }

    public static void reset() {
        setUpgradeFireRate(0);
        setUpgradeSpeed(0);
        setShield(0);
        setScore(0);
        GameScreen.setShieldActive(1);
        reinitialiseStats();
    }
}


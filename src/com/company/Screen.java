package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**      _________  _____ _____  ______ ______ _   _
 *      / / / ____|/ ____|  __ \|  ____|  ____| \ | |
 *     / / / (___ | |    | |__) | |__  | |__  |  \| |
 *    / / / \___ \| |    |  _  /|  __| |  __| | . ` |
 *   / / /  ____) | |____| | \ \| |____| |____| |\  |
 *  /_/_/  |_____/ \_____|_|  \_\______|______|_| \_|
 *
 * Screen Class
 */

class Screen extends JFrame {
    //--------------------------------------------------------------------------------------------------//
    /**Width and height for the screen. These can be modified at program startup by screen select class*/
    //--------------------------------------------------------------------------------------------------//
    public static int width = 800, height = 600;
    /*****************************************END DECLARATIONS******************************************/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function performs all of the neccesary modifications to a standard JFrame to fit the games requirements*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void init() {
        this.setSize(width, height);
        this.setResizable(false);
        Dimension SystemScreen = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(SystemScreen.width / 2 - 300, SystemScreen.height / 2 - 400);
        this.setLayout(new CardLayout());
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function handles screen changes from button presses as well as player movements*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    Screen(){
        this.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

	            char ch = e.getKeyChar();
	            int code = e.getKeyCode();
	            if (ch == 'd' || ch == 'D') {

		            Main.setRightKey(true);
		            Player.setTurnRight(true);
	            }
	            if (ch == 'a' || ch == 'A') {
		            Main.setLeftKey(true);
		            Player.setTurnLeft(true);
	            }
	            if (ch == 's'|| ch == 'S') {
		            Main.setShoot(true);
		            Main.getFlag()[5] = false;
		            Main.getFlag()[7] = false;
	            }
	            if (ch == 'p'|| ch == 'P') {
		            Main.getFlag()[5] = true;
	            }
	            if (ch == 27) {
		            System.exit(1);
	            }
	            if (ch == 'c' && Main.getFlag()[6]) {
                    Main.resetFlags();
		            Main.getFlag()[0] = true;
	            }
	            //This is for direct movement to upgrade screen DEBUG ONLY
	            if (code == KeyEvent.VK_PAGE_UP) {
		            Main.resetFlags();
		            Main.getFlag()[4] = true;
	            }
	            if (ch == 'o') {
                    Main.resetFlags();
		            Main.getFlag()[1] = true;
	            }
				if (code == KeyEvent.VK_PAGE_DOWN){
		            Main.resetFlags();
		            Main.getFlag()[6] = true;
	            }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                char ch = e.getKeyChar();
                if (ch == 'd'|| ch == 'D') {
                    Main.setRightKey(false);
                    Player.setTurnRight(false);
                }
                if (ch == 'a'|| ch == 'A') {
                    Main.setLeftKey(false);
                    Player.setTurnLeft(false);
                }
                if (ch == 's'|| ch == 'S') {
                    Main.setShoot(false);
                }
            }
        });
    }
}

package com.company;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
/**
 *       _________                         _    _
 *      / / /  __ \                       | |  | |
 *     / / /| |__) |____      _____ _ __  | |  | |_ __  ___
 *    / / / |  ___/ _ \ \ /\ / / _ \ '__| | |  | | '_ \/ __|
 *   / / /  | |  | (_) \ V  V /  __/ |    | |__| | |_) \__ \
 *  /_/_/   |_|   \___/ \_/\_/ \___|_|     \____/| .__/|___/
 *                                               | |
 *                                               |_|
 *
 * Class that handles the power ups that drop when enemies die
 */
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
class PowerUp {

	//-------------------------------------------------------------------------------------------//
	/**Initialise position and size of power up*/
	//-------------------------------------------------------------------------------------------//
    private int x = 0;
    private int y = 0;
	//-------------------------------------------------------------------------------------------//
	/**Initialise which power up to use*/
	//-------------------------------------------------------------------------------------------//
	private int powerNumber = -1;
	//-------------------------------------------------------------------------------------------//
	/**Create array of strings of the different power ups filenames*/
	//-------------------------------------------------------------------------------------------//
	private final String[] powerUpImages = {"PowerupSUP.png", "PowerupR.png", "PowerupSDOWN.png", "PowerupL.png", "PowerupHP.png"};
	//-------------------------------------------------------------------------------------------//
	/**Initialise power up image*/
	//-------------------------------------------------------------------------------------------//
	private Image filepath;

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Get and Set methods*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getPowerNumber() {
		return powerNumber;
	}
	public int getWidth() {
		int width = 15;
		return width; }
	private int getHeight() {
		int height = 15;
		return height; }
	private void setPowerNumber(int powerNumber) {
		this.powerNumber = powerNumber;
	}
	private String[] getPowerUpImages() {
		return powerUpImages;
	}
	public Image getFilepath() {
		return filepath;
	}
	private void setFilepath(Image filepath) {
		this.filepath = filepath;
	}

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Constructor for Power up passed which power up is to be used*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public PowerUp(int number) {
        setPowerNumber(number);
	    try {
		    setFilepath(ImageIO.read(new File("images\\" + getPowerUpImages()[getPowerNumber() - 1])));
	    } catch (IOException e) {
		    e.printStackTrace();
	    }
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Function that calls appropriate power for the current power up*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    void givePower(){
	    //power ups also add to score
	    Player.setScore(Player.getScore() + 20 * GameScreen.getLevel());
	    if (getPowerNumber() == 1) {
            increaseSpeed();
        } else if (getPowerNumber() == 2) {
            increaseFireRate();
        } else if (getPowerNumber() == 3) {
            decreaseSpeed();
        } else if (getPowerNumber() == 4) {
            addLife();
        } else if (getPowerNumber() == 5) {
            addHealth();
        }
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Increase speed power up - resets firerate power up level*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private void increaseSpeed() {
        Player.setPowerUpSpeed(5.0);
	    Player.setPowerUpFireRate(1);
        Player.reinitialiseStats();
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Increase fire rate power up - resets speed power up level*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private void increaseFireRate() {
	    Player.setPowerUpSpeed(0);
        Player.setPowerUpFireRate(3);
        Player.reinitialiseStats();
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**decrease speed power up - resets firerate power up level*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private void decreaseSpeed() {
        Player.setPowerUpSpeed(0);
	    Player.setPowerUpFireRate(1);
        Player.reinitialiseStats();
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Add 1 to players lives*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private void addLife() {
        Player.setLives(Player.getLives() + 1);
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Increase Players health by a third up to a maximum of 100*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private void addHealth() {
        if (Player.getHealth() <= 67)
            Player.setHealth(Player.getHealth() + 33);
        else {
            Player.setHealth(100);
        }
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**function makes power up move down the screen*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    void powerUpMove() {
        int dropSpeed = 2;
        setY(getY() + dropSpeed);
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Function that creates outline of power up for collision detection*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    Rectangle createRect() {
        return new Rectangle(getX(), getY(), getWidth(), getHeight());
    }

}

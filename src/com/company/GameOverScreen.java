package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
/**
 *      _________                       ____                 _____
 *     / / / ____|                     / __ \               / ____|
 *    / / / |  __  __ _ _ __ ___   ___| |  | |_   _____ _ _| (___   ___ _ __ ___  ___ _ __
 *   / / /| | |_ |/ _` | '_ ` _ \ / _ \ |  | \ \ / / _ \ '__\___ \ / __| '__/ _ \/ _ \ '_ \
 *  / / / | |__| | (_| | | | | | |  __/ |__| |\ V /  __/ |  ____) | (__| | |  __/  __/ | | |
 * /_/_/   \_____|\__,_|_| |_| |_|\___|\____/  \_/ \___|_| |_____/ \___|_|  \___|\___|_| |_|
 *
 *  Creates the Game Over screen to be displayed when all lives lost or enemies collide with player
 */
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

class GameOverScreen extends JPanel {

	//-------------------------------------------------------------------------------------------//
	/**Set play button*/
	//-------------------------------------------------------------------------------------------//
	private final JButton play = new JButton("Play");
	//-------------------------------------------------------------------------------------------//
	/**Set exit button*/
	//-------------------------------------------------------------------------------------------//
    private final JButton exit = new JButton("Exit");
	//-------------------------------------------------------------------------------------------//
	/**Background image*/
	//-------------------------------------------------------------------------------------------//
    private static Image bgbuffer ;

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Constructor for game over screen - deals with exceptions*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    GameOverScreen() throws IOException{
        this.setLayout(new GridBagLayout());
	    getPlay().setLocation(100, 100);
        getExit().setLocation(200, 200);
        String background = "images//gameover.png";
        setBgbuffer(ImageIO.read(new File(background)));
        setBgbuffer(getBgbuffer().getScaledInstance(Screen.width, (int) (Screen.height * 0.97), Image.SCALE_DEFAULT));
    }

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Get and Set methods*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static Image getBgbuffer() {
        return bgbuffer;
    }
    private static void setBgbuffer(Image bgbuffer) {
        GameOverScreen.bgbuffer = bgbuffer;
    }
	private JButton getPlay() {
		return play;
	}
	private JButton getExit() {
		return exit;
	}

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	/**Loads images and paints screen*/
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;
        this.setBackground(Color.BLACK);
	    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    //set text colour and font
	    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    g2.setColor(Color.white);
        g2.setFont(new Font("Dialog", Font.BOLD, 65));
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    //draw background image
	    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    this.setBackground(new Color(0, 0, 0, 0));
        g2.drawImage(getBgbuffer(), 0, 0, null);
	    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    //Write score and return instruction
	    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
	    String score = Integer.toString(Player.getScore());
        g2.drawString(" " + Player.getScore(), Screen.width/2 - score.length()*30, Screen.height/2);
	    g2.setFont(new Font("Dialog", Font.ITALIC , 30));
	    g2.drawString("Press c to return to menu", Screen.width/4, Screen.height/2 + 80);
    }

}

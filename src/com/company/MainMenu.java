package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
/**       ______  __          _____ _   _   __  __ ______ _   _ _    _
 *       / / /  \/  |   /\   |_   _| \ | | |  \/  |  ____| \ | | |  | |
 *      / / /| \  / |  /  \    | | |  \| | | \  / | |__  |  \| | |  | |
 *     / / / | |\/| | / /\ \   | | | . ` | | |\/| |  __| | . ` | |  | |
 *    / / /  | |  | |/ ____ \ _| |_| |\  | | |  | | |____| |\  | |__| |
 *   /_/_/   |_|  |_/_/    \_\_____|_| \_| |_|  |_|______|_| \_|\____/
 *  Class handles the main menu
 **/
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

class MainMenu extends JPanel {

    //--------------------------------------------------------------------------------------------------//
    /**String to hold location of the menu image*/
    //--------------------------------------------------------------------------------------------------//
    private static  final   String  menuImage   = "images\\MainMenu.png";
    //--------------------------------------------------------------------------------------------------//
    /**Create menu image holder*/
    //--------------------------------------------------------------------------------------------------//
    private static          Image   menuBuffer;
    //--------------------------------------------------------------------------------------------------//
    /**Create buttons and set them to the appropriate images*/
    //--------------------------------------------------------------------------------------------------//
    private         final   JButton play        = new JButton(new ImageIcon("images\\playButton.png"));
    private         final   JButton load        = new JButton(new ImageIcon("images\\Load.png"));
    private         final   JButton exit        = new JButton(new ImageIcon("images\\exitButton.png"));
    private         final   JButton option      = new JButton(new ImageIcon("images\\options.png"));
    private         final   JButton tutorial    = new JButton(new ImageIcon("images\\tutorial1.png"));
    private         final   JButton survival    = new JButton(new ImageIcon("images\\Survival.png"));
    //--------------------------------------------------------------------------------------------------//
    /*****************************************END DECLARATIONS******************************************/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Constructor initialises all buttons and sets their locations. Also sets screen layout and shows background image*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    MainMenu() throws IOException {
        /**
         * For each button: Set background clear, set border
         * to not show up and add the mouse listener for
         * hover colour
         */
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise play button*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        getPlay().setBackground(new Color(0, 0, 0, 0));
        getPlay().setBorderPainted(false);
        getPlay().addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise exit button*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        getExit().setBackground(new Color(0, 0, 0, 0));
        getExit().setBorderPainted(false);
        getExit().addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise option button*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        getOption().setBackground(new Color(0, 0, 0, 0));
        getOption().setBorderPainted(false);
        getOption().addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise load button*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        getLoad().setBackground(new Color(0, 0, 0, 0));
        getLoad().setBorderPainted(false);
        getLoad().addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise tutorial button*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        getTutorial().setBorderPainted(false);
        getTutorial().setBackground(new Color(0, 0, 0, 0));
        getTutorial().addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initialise survival mode button*/
        /*
        * Also adds an Action Listener for survival button
        * click action
        */
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        getSurvival().addActionListener(new SurvivalListener());
        getSurvival().setBorderPainted(false);
        getSurvival().setBackground(new Color(0, 0, 0, 0));
        getSurvival().addMouseListener(new MouseOver());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Set the layout to grid bag layout*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        this.setLayout(new GridBagLayout());
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Set the constraints of the grid to hold the buttons*/
        /*Adds the buttons*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        GridBagConstraints c = new GridBagConstraints();
        c.weightx   = 0.5;
        c.gridx     = 0;
        c.gridy     = 0;
        c.insets    = new Insets((int)(0.20 * Screen.height), 0, 0, 0);
        this.add(getPlay(), c);
        c.gridy     = 2;
        c.insets    = new Insets(0,0,0,0);
        this.add(getTutorial(), c);
        getTutorial().addActionListener(new tut());
        c.gridy     = 1;
        c.insets    = new Insets(0,(int)(Screen.width * 0.50),0,0);
        this.add(getLoad(), c);
        c.insets    = new Insets(0,0,0,(int)(Screen.width * 0.50));
        this.add(getSurvival(),c);
        c.insets    = new Insets(0,0,0,0);
        c.gridy     = 3;
        this.add(getOption(), c);
        c.gridy     = 4;
        this.add(getExit(), c);
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Read the menu background and load it in the back*/
        /*Scale image*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        setMenuBuffer(ImageIO.read(new File(getMenuImage())));
        setMenuBuffer(getMenuBuffer().getScaledInstance(Screen.width, (int) (Screen.height * 0.97), Image.SCALE_DEFAULT));
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Getter for the menu image file path as a string*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static String getMenuImage() {
        return menuImage;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Getter for the menu image*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static Image getMenuBuffer() {
        return menuBuffer;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Setter for the menu image*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static void setMenuBuffer(Image menuBuffer) {
        MainMenu.menuBuffer = menuBuffer;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Paint method: This loads the background and sets the background color/image*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void paintComponent(Graphics g) {
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Initial stuff for painting components*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Setting the background to clear*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        this.setBackground(new Color(0, 0, 0, 0));
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        /*Drawing the background image*/
        //++++++++++++++++++++++++++++++++++++++++++++++++++//
        g2.drawImage(getMenuBuffer(), 0, 0, null);
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**The following functions are the getters for the JButtons*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public JButton  getPlay() {
        return play;
    }
    public JButton  getLoad() {
        return load;
    }
    public JButton  getExit() {
        return exit;
    }
    public JButton  getOption() {
        return option;
    }
    private JButton getTutorial() {
        return tutorial;
    }
    private JButton getSurvival() {
        return survival;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**The following class is an action listener to initialise the tutorial*/
    /**Starts the tutorial thread*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private class tut implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Main.resetFlags();
            Main.getFlag()[3] = true;
            GameScreen.enemyInit();
            new Thread(new Tutorial()).start();
        }
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**The following class is an action listener to initialise survival mode*/
    /**Sets the mode true*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private class SurvivalListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Main.resetFlags();
            Main.getFlag()[3] = true;
            Enemy.setStatic_y(0);
            Main.dir = 10;
            GameScreen.enemyInit();
            GameScreen.setMode(true);
        }
    }
}

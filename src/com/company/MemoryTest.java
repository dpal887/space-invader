package com.company;

import javax.swing.*;
import java.awt.*;

/**      ______  __ ______ __  __  ____  _______     __  _______ ______  _____ _______
 *      / / /  \/  |  ____|  \/  |/ __ \|  __ \ \   / / |__   __|  ____|/ ____|__   __|
 *     / / /| \  / | |__  | \  / | |  | | |__) \ \_/ /     | |  | |__  | (___    | |
 *    / / / | |\/| |  __| | |\/| | |  | |  _  / \   /      | |  |  __|  \___ \   | |
 *   / / /  | |  | | |____| |  | | |__| | | \ \  | |       | |  | |____ ____) |  | |
 *  /_/_/   |_|  |_|______|_|  |_|\____/|_|  \_\ |_|       |_|  |______|_____/   |_|
 *
 */

class MemoryTest implements Runnable {

    private static String memUseage;
    private static int prevmemUseage;

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Getter and Setter method for memUsage*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static String getMemUseage() {
        return memUseage;
    }
    private static void setMemUseage(String memUseage) {
        MemoryTest.memUseage = memUseage;
    }

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**Getter and Setter method for prevmemUsage*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static int getPrevmemUseage() {
        return prevmemUseage;
    }
    private static void setPrevmemUseage(int prevmemUseage) {
        MemoryTest.prevmemUseage = prevmemUseage;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function gets a few basic memory statistics and relays them on a JPanel*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void run() {

        JWindow frame = new JWindow();
        frame.setSize(300, 300);
        frame.setLayout(new CardLayout());
        frame.setBackground(new Color(0, 0, 0, 0));
        JPanel panel = new JPanel() {
            public void paint(Graphics g) {
                super.paint(g);
                Graphics2D g2 = (Graphics2D) g;
                this.setBackground(new Color(0, 0, 0, 0));
                g2.setColor(Color.blue);
                g2.fillRect(0, 0, 300, 100);
                g2.setFont(new Font("Standard", Font.BOLD, 20));
                if((int) (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024 > getPrevmemUseage()) {
                    g2.setColor(Color.red);
                }else {
                    g2.setColor(Color.green);
                }
                g2.drawString("Used Memory     : " + getMemUseage(), 0, 20);
                setPrevmemUseage((int) (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024);
                g2.drawString("Available Memory: " + Runtime.getRuntime().freeMemory(), 0, 40);
                g2.drawString("Memory Percent  : " + (int)(100.0 - (Runtime.getRuntime().freeMemory()/(Runtime.getRuntime().totalMemory()/1.24) * 100)) + "%", 0, 60);
                g2.drawString("Available CPU's : " + Runtime.getRuntime().availableProcessors(), 0, 80);
            }
        };
        frame.add(panel);
        panel.setVisible(true);
        frame.setVisible(true);

        while (true) {
            setMemUseage("KB: " + (int) (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024);
            Intro.setMemory(getMemUseage());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            frame.repaint();
            panel.repaint();
        }
    }
}

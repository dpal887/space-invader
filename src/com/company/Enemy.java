package com.company;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
/**       __________ _   _ ______ __  ____     __
 *       / / /  ____| \ | |  ____|  \/  \ \   / /
 *      / / /| |__  |  \| | |__  | \  / |\ \_/ /
 *     / / / |  __| | . ` |  __| | |\/| | \   /
 *    / / /  | |____| |\  | |____| |  | |  | |
 *   /_/_/   |______|_| \_|______|_|  |_|  |_|
 *  Class handles the enemies to be created in the game
 **/
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

class Enemy {
    //---------------------------------------------------------------------------//
    /**Enemy Dimensions: Width & Height*/
    //---------------------------------------------------------------------------//
    private static final int                 height      = Screen.height / 30;
    private static final int                 width       = Screen.width / 15;
    //---------------------------------------------------------------------------//

    /**Static enemy vector positions*/
    //---------------------------------------------------------------------------//
    private static          int                 static_y    = 0;
    //---------------------------------------------------------------------------//
    /**Non-Static individual enemy vector positions*/
    //---------------------------------------------------------------------------//
    private                 int                 x           = 0;
    private                 int                 y           = 40;
    //---------------------------------------------------------------------------//
    /**Static enemy movement speed*/
    //---------------------------------------------------------------------------//
    private static          int                 speed       = 10;
    //---------------------------------------------------------------------------//
    /**Individual enemy health declaration*/
    //---------------------------------------------------------------------------//
    private                 int                 health      = 100;
    //---------------------------------------------------------------------------//
    /**Individual enemy images - not static as needs to change on hit */
    //---------------------------------------------------------------------------//
    private                 Image               enemyImage;
    //---------------------------------------------------------------------------//
    /**Static image imports for enemy colours */
    //---------------------------------------------------------------------------//
    private static          Image               enemyImage1;
    private static          Image               enemyImage2;
    private static          Image               enemyImage3;
    private static          Image               enemyImage4;
    private static          Image               enemyImage5;
    private static          Image               enemyImage6;
    //---------------------------------------------------------------------------//
    /**Arrays to hold individual enemy bullet positions */
    //---------------------------------------------------------------------------//
    private         final   ArrayList<Bullet>   activeBullets = new ArrayList<Bullet>(0);
    //---------------------------------------------------------------------------//
    /**Enemy image to show as chosen by gamescreen and is a variable*/
    /** relative to enemy health                                    */
    //---------------------------------------------------------------------------//
    private                 int                 n           = 1;
    //---------------------------------------------------------------------------//
    /*****************************************END DECLARATIONS******************************************/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function initialises the enemy. Importing all the images once and scaling them to size*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public static void init() {
        setEnemyImage1(new ImageIcon(getBarrierImage() + 1 + ".png").getImage());
        setEnemyImage1(getEnemyImage1().getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH));
        setEnemyImage2(new ImageIcon(getBarrierImage() + 2 + ".png").getImage());
        setEnemyImage2(getEnemyImage2().getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH));
        setEnemyImage3(new ImageIcon(getBarrierImage() + 3 + ".png").getImage());
        setEnemyImage3(getEnemyImage3().getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH));
        setEnemyImage4(new ImageIcon(getBarrierImage() + 4 + ".png").getImage());
        setEnemyImage4(getEnemyImage4().getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH));
        setEnemyImage5(new ImageIcon(getBarrierImage() + 5 + ".png").getImage());
        setEnemyImage5(getEnemyImage5().getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH));
        setEnemyImage6(new ImageIcon(getBarrierImage() + 6 + ".png").getImage());
        setEnemyImage6(getEnemyImage6().getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH));
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This is the function for getting initial extension for getting the space invader image*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static String getBarrierImage() {
        return "images\\spaceinvader";
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This is the function for getting the static y vector of the enemy*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public static int getStatic_y() {
        return static_y;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This is the function for setting the static y vector*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public static void setStatic_y(int static_y) {
        Enemy.static_y = static_y;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This is the function for getting the enemy speed*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public static int getSpeed() {
        return speed;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This is the function for setting the enemy speed*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public static void setSpeed(int speed) {
        Enemy.speed = speed;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**These are the functions for getting and setting each static enemy image*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private static Image getEnemyImage1() {
        return enemyImage1;
    }
    private static void setEnemyImage1(Image enemyImage1) {
        Enemy.enemyImage1 = enemyImage1;
    }
    private static Image getEnemyImage2() {
        return enemyImage2;
    }
    private static void setEnemyImage2(Image enemyImage2) {
        Enemy.enemyImage2 = enemyImage2;
    }
    private static Image getEnemyImage3() {
        return enemyImage3;
    }
    private static void setEnemyImage3(Image enemyImage3) {
        Enemy.enemyImage3 = enemyImage3;
    }
    private static Image getEnemyImage4() {
        return enemyImage4;
    }
    private static void setEnemyImage4(Image enemyImage4) {
        Enemy.enemyImage4 = enemyImage4;
    }
    private static Image getEnemyImage5() {
        return enemyImage5;
    }
    private static void setEnemyImage5(Image enemyImage5) {
        Enemy.enemyImage5 = enemyImage5;
    }
    private static Image getEnemyImage6() {
        return enemyImage6;
    }
    private static void setEnemyImage6(Image enemyImage6) {
        Enemy.enemyImage6 = enemyImage6;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This is the function for updating which image the enemy is showing based on how much health it has lost*/
    /**This is based on the value n which is set in gamescreen based on the enemy health*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void update() {

        switch (getN()){
            case 1: setEnemyImage(getEnemyImage1());
                break;
            case 2: setEnemyImage(getEnemyImage2());
                break;
            case 3: setEnemyImage(getEnemyImage3());
                break;
            case 4: setEnemyImage(getEnemyImage4());
                break;
            case 5: setEnemyImage(getEnemyImage5());
                break;
            case 6: setEnemyImage(getEnemyImage6());
                break;
        }
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function moves the enemy left or right depending on the integer dir*/
    /**It also moves the enemies up or down depending on the static integer Static_y*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void move(int dir) {
        setX(getX() + (dir));
        setY(getY() + getStatic_y());
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function causes the enemy to shoot a new bullet.*/
    /**The initial bullet position should be set at the centre of the enemy*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void shoot() {
        Standard_Bullet shot = new Standard_Bullet(0);
        shot.setXPos(getX() + getWidth() / 2);
        shot.setYPos(getY());
        getActiveBullets().add(0, shot);
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function gets the enemy height*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    static int getHeight() {
        return height;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function gets the enemy width*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    static int getWidth() {
        return width;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function gets the enemy image*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public Image getEnemyImage() {
        return enemyImage;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function sets the enemy image*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private void setEnemyImage(Image enemyImage) {
        this.enemyImage = enemyImage;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function gets the enemies active bullets*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public ArrayList<Bullet> getActiveBullets() {
        return activeBullets;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function gets the enemies individual health*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public int getHealth() {
        return health;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function sets the enemies individual health*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void setHealth(int health) {
        this.health = health;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function gets the enemies individual x position*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public int getX() {
        return x;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function sets the enemies individual x position*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void setX(int x) {
        this.x = x;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function gets the enemies individual y position*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public int getY() {
        return y;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function sets the enemies individual y position*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void setY(int y) {
        this.y = y;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function gets the variable n to select which enemy image to show*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    private int getN() {
        return n;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    /**This function sets the variable n to select which enemy image to show*/
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    public void setN(int n) {
        this.n = n;
    }
}

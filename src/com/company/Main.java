package com.company;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;


/**
 *    _____ _____        _____ ______   _____ _   ___      __     _____  ______ _____   _____
 *   / ____|  __ \ /\   / ____|  ____| |_   _| \ | \ \    / /\   |  __ \|  ____|  __ \ / ____|
 *  | (___ | |__) /  \ | |    | |__      | | |  \| |\ \  / /  \  | |  | | |__  | |__) | (___
 *   \___ \|  ___/ /\ \| |    |  __|     | | | . ` | \ \/ / /\ \ | |  | |  __| |  _  / \___ \
 *  _ ___) | |  / ____ \ |____| |____   _| |_| |\  |  \  / ____ \| |__| | |____| | \ \ ____) |
 *  |_____/|_| /_/    \_\_____|______| |_____|_| \_|   \/_/    \_\_____/|______|_|  \_\_____/
 * Daniel Paley
 * Finn Kennedy
 * Sunny Wang
 *
 * ENGINE VERSION 5.8.1
 *
 *
 */

class Main {
    private static final boolean[]   Flag        = {true,false,true,false,false,false,false,false};
    private static       int         soundtrack  = 0;
    private static       boolean     leftKey;
    private static       boolean     rightKey;
    private static       boolean     shoot;
    private static       boolean     spawn       = false;
    private static       int         start       = 0;
    private static       int         count       = 0;
    public  static       int         dir         = 10;                                                                   //Variable responsible for movements of the enemy.//

    /**This is the function that the game runs from*/
    public static void main(String[] args) throws InterruptedException, IOException, LineUnavailableException, UnsupportedAudioFileException {

        /** Declaration of local variables */
        int enemyMoveCounter                    = 0;                                                                    //Counter to slow enemy movements. This works like a clock divider//
        int collision;

        /** Change the button select colour to clear*/
        UIManager.put("Button.select", new Color(255, 0, 0, 100));

        /**Initialise the sound manager: This class manages the background music in the game */
        //-----------------------------------------------------------//
        SoundMgr mgr                            = new SoundMgr();
        Thread music                            = new Thread(mgr);                                                      //Create thread for sound manager to run from//
        music.start();                                                                                                  //Start the music//
        //-----------------------------------------------------------//

        /**Initialise Screen selection : This class manages the size of the Game Play screen. Two options available*/
        //-----------------------------------------------------------//
        ScreenSelect screenSelect               = new ScreenSelect();

        screenSelect.init();                                                                                            //Create a new resolution option screen
        while (getFlag()[2]) {                                                                                               //This initialises the screen to show resolution options. Currently only supporting 600x800 and 800*600
            Thread.sleep(1);                                                                                            //Wait until an option is chosen and ok button is pressed
            screenSelect.checkScreenStatus();
        }
        //-----------------------------------------------------------//

        /**The following sequence runs the splash screen*/
        //-----------------------------------------------------------//
        setSoundtrack(1);
        Thread t                        = new Thread(new Initialise());                                                 //Add the thread to run the splash screen
        Thread t2                       = new Thread(new MemoryTest());                                                 //Add the thread to attach to memory.
        t.start();                                                                                                      //Start the splash screen
        t2.start();                                                                                                     //Start the debug module
        while (t.isAlive()) {                                                                                           //While the splash screen is active, wait.
            Thread.sleep(1);
        }
        //-----------------------------------------------------------//

        /**Initialise JFrames to hold the game*/
        //-----------------------------------------------------------//
        Screen screen                   = new Screen();                                                                 //Create a screen
        screen.setFocusable(true);                                                                                      //Added this so that I don't have to click out of the screen all the time
        //-----------------------------------------------------------//

        /**Initialise JPanels to be run within the game*/
        //-----------------------------------------------------------//
        MainMenu menu                   = new MainMenu();       /**FLAG 0*/                                             //Create a main menu
        OptionsScreen options           = new OptionsScreen();  /**FLAG 1*/                                             //Create an options menu
        GameScreen game                 = new GameScreen();     /**FLAG 3*/                                             //Create a game screen
        UpgradeScreen upgrade           = new UpgradeScreen();  /**FLAG 4*/                                             //Create the upgrade screen
        GameOverScreen gameOver         = new GameOverScreen(); /**FLAG 6*/                                             //Add the game over screen
        //-----------------------------------------------------------//

        /**Initialises Both Player and Enemy*/
        //-----------------------------------------------------------//
        Enemy.init();                                                                                                   //Initialise the enemy
        Player.init();                                                                                                  //Initialise the player
        //-----------------------------------------------------------//

        /**Action listeners to be used in the main menu*/
        //-----------------------------------------------------------//
        menu.getPlay().addActionListener(new PlayListener());                                                                //Add listeners to buttons on screens
        menu.getOption().addActionListener(new OptListener());
        menu.getExit().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        menu.getLoad().addActionListener(new LoadListener());
        //-----------------------------------------------------------//

        /**Action listeners to be used in the upgrade menu*/
        //-----------------------------------------------------------//
        upgrade.play.addActionListener(new PlayListener());
        upgrade.exit.addActionListener(new ExitListener());
        //-----------------------------------------------------------//

        /**Add panels to screen for use with card layout*/
        //-----------------------------------------------------------//
        screen.add(menu);
        screen.add(game);
        screen.add(upgrade);
        screen.add(gameOver);
        screen.add(options);
        //-----------------------------------------------------------//

        /**Final initialisations*/
        //-----------------------------------------------------------//
        screen.init();                                                                                                  //Initialise and open the JFrame
        setSoundtrack(2);                                                                                               //Set the soundtrack to track 2
        //-----------------------------------------------------------//

//******************************************END OF INITIALISATION*******************************************************//

        /**
         * Engine Specifics
         * A series of flags control the current active screen.
         * Each panel is controlled within a while loop.
         * A new panel is shown when the program exits a while loop and enters another one.
         * Flags are controlled by key-presses, button presses, and game-play events.
         * DEMO
         *
         * while(true)
         *  if(Flag is set)
         *      ->Kill all screens
         *      ->Start wanted screen
         *      while(Same Flag is set)
         *          ->Run Screen operations
         *
         *  if(Another Flag)....
         *  ...
         *  ... e.t.c.
         * End Loop
         **/


//********************************************BEGIN GAME ENGINE*********************************************************//
        while (true) {
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
            /**        _________          __  __ ______    _____  _____ _____  ______ ______ _   _
             *        / / / ____|   /\   |  \/  |  ____|  / ____|/ ____|  __ \|  ____|  ____| \ | |
             *       / / / |  __   /  \  | \  / | |__    | (___ | |    | |__) | |__  | |__  |  \| |
             *      / / /| | |_ | / /\ \ | |\/| |  __|    \___ \| |    |  _  /|  __| |  __| | . ` |
             *     / / / | |__| |/ ____ \| |  | | |____   ____) | |____| | \ \| |____| |____| |\  |
             *    /_/_/   \_____/_/    \_\_|  |_|______| |_____/ \_____|_|  \_\______|______|_| \_|
             *                This is the game-play screen and handles the actual UI
             *
             **/
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
            if (getFlag()[3]) {                                      /**Game Flag*/
                menu.setVisible(false);                                                                                 //Set up which panel is on top
                upgrade.setVisible(false);
                game.setVisible(true);
                options.setVisible(false);
                getFlag()[7] = true;                                 /**Initial Pause Flag*/                                 //Initial state is semi - paused - Player can move left or right
                while (getFlag()[3]) {                               /**Game Flag*/                                          //The game loop runs the game-screen
                    /**Game starts in initial pause state*/
                    //****************************************************************//
                    while (getFlag()[7]) {                           /**Initial pause flag*/                                 //While game-screen is still running implement the game-play sequence
                        Thread.sleep(10);
                        game.repaint();
                        if (isRightKey()) {                                                                                 //Check for keypresses
                            Player.moveRight();                                                                         //Move the player right on keypress
                        }
                        if (isLeftKey()) {
                            Player.moveLeft();                                                                          //Move the player left on keypress
                        }
                        GameScreen.setNyn_flag(true);
                    }
                    //****************************************************************//
                    /**This internal loop handles do-nothing inside pause loop*/
                    //****************************************************************//
                    while (getFlag()[5]){
                        Thread.sleep(1);                                                                                //Use sleep as a pause
                    }
                    //****************************************************************//
                    game.repaint();                                                                                     //Re draw the page
                    /**
                     *Enemies will move once every 10 thread sleeps
                     *i.e. count up each iteration and when it hits 10 move the enemy once
                     *
                     * (1) Check counter = 10 {If counter = 10 goto (2) else goto (4)}
                     * (2) Set enemy static Y position to 0 (Static Y position is a modifier not a set value of the enemy){goto (3)}
                     * (3) Set collision to -1 i.e. no clear collision flag. Collision relates to enemy hitting the wall.{goto (4)}
                     * (3-15) Loop through all enemies:
                     * (4) Random chance for enemy to fire a bullet{true => goto (5) false => goto (6)}
                     * (5) Enemy Fires{goto (6)
                     * (6) Check if the enemy requires a correction movement {true => goto (7) false => goto (8)}
                     * (7) Move the enemy{goto (8)}
                     * (8) Move the enemy{goto (9)}
                     * (9) Check if an enemy hits the left OR right side of the screen {true => goto (10) false => goto =>(14)}
                     * (10)Change enemy direction{goto (11)}
                     * (11)Tell enemy to move down 20 pixels once{goto (12)}
                     * (12)Set the collision variable (how many enemies will miss out on the next movement) for patch in (6){goto 13}
                     * (13)Set i to -1 i.e. restart the loop. (CAUSED BUG WHICH HAS BEEN PATCHED IN (6) and (12)){goto (14)}
                     * (14)Set the movement counter back to 0 {goto (1)}
                     * (15)If the player didn't move increment the counter {goto (1)}
                     */
                    //****************************************************************//
                    if (enemyMoveCounter == (Enemy.getSpeed())){                                                             //(1)
                        Enemy.setStatic_y(0);                                                                             //(2)
                        collision = -1;                                                                                 //(3)

                        for (int i = 0; i < GameScreen.getEnemy().size(); i++) {                                             //Loop through each enemy
                            if (Math.random()  > 0.99 - GameScreen.getLevel() * 0.01) {                                      //(4)
                                GameScreen.getEnemy().get(i).shoot();                                                        //(5)
                            }
                            if (i <= collision) {                                                                       //(6)
                                GameScreen.getEnemy().get(i).setX(GameScreen.getEnemy().get(i).getX() + dir);                            //(7)
                            }
                            GameScreen.getEnemy().get(i).move(dir);                                                          //(8)
                            if ((GameScreen.getEnemy().get(i).getX() > (Screen.width - Enemy.getWidth())) || GameScreen.getEnemy().get(i).getX() < 0) { //(9)
                                dir = -dir;                                                                             //(10)
                                Enemy.setStatic_y(Enemy.getStatic_y() + 20);                                                                   //(11)
                                collision = i;                                                                          //(12)
                                i = -1;                                                                                 //(13)
                                if (GameScreen.isMode()){
                                    setCount(getCount() + 1);
                                }
                            }
                        }
                        enemyMoveCounter = 0;                                                                           //(14)
                    } else if(enemyMoveCounter == 10){
                        enemyMoveCounter = 0;
                    }
                    else {
                        enemyMoveCounter ++;                                                                            //(15)
                    }
                    //****************************************************************//
                    /**Handles cooldown on bullets and movement of the player*/
                    //****************************************************************//
                    Player.countDown();                                                                                 //Player bullet cooldown
                    if (isRightKey()) {                                                                                     //Check for keypresses
                        Player.moveRight();                                                                             //Move the player right on keypress
                    }
                    if (isLeftKey()) {
                        Player.moveLeft();                                                                              //Move the player left on keypress
                    }
                    if (isShoot()) {
                        Player.shoot();                                                                                 //Make the player shoot a bullet
                        Player.getSound().start();                                                                           //Start a shoot sound
                    }
                    if(!isShoot()){
                        Player.getSound().stop();                                                                            //Stop the players shoot sound
                    }
                    //****************************************************************//
                    /**Handles in game triggers for new screens*/
                    //****************************************************************//
                    if (GameScreen.getEnemy().size() <= 0 && !GameScreen.isMode()) {                                             //Check game-screen finished (all enemies shot) and if so go to upgrade screen
                        resetFlags();
                        getFlag()[4] = true;
                    }
                    else if(GameScreen.getEnemy().size() <= 0 && GameScreen.isMode()){
                        Enemy.setStatic_y(0);
                        GameScreen.enemyInit();                                                                         //Initialise a new row of enemies if survival mode is actived
                    }
                    if (game.isPlayerDead()) {                                                                              //Kill game and send to game over screen
                        resetFlags();
                        getFlag()[6] = true;
                    }

                    if (GameScreen.isMode()) {
                        if (getCount() % 2 == 0 && getCount() != 0){
                            setStart(getStart() + 1);
                                if (getStart() == 54) {
                                    setSpawn(true);
                                    continue;
                                }
                            }

                        else {
                            setStart(0);
                            setSpawn(false);
                        }
                    }
                    Thread.sleep(10);
                }
            }
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
            /**
             *           ______  __ ______ _   _ _    _    _____  _____ _____  ______ ______ _   _
             *          / / /  \/  |  ____| \ | | |  | |  / ____|/ ____|  __ \|  ____|  ____| \ | |
             *         / / /| \  / | |__  |  \| | |  | | | (___ | |    | |__) | |__  | |__  |  \| |
             *        / / / | |\/| |  __| | . ` | |  | |  \___ \| |    |  _  /|  __| |  __| | . ` |
             *       / / /  | |  | | |____| |\  | |__| |  ____) | |____| | \ \| |____| |____| |\  |
             *      /_/_/   |_|  |_|______|_| \_|\____/  |_____/ \_____|_|  \_\______|______|_| \_|
             *
             *
             **/
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
            if (getFlag()[0]/*menu flag*/) {                                                                                 //This loop handles the menu
                GameScreen.setLevel(1);
                Player.reset();
                gameOver.setVisible(false);                                                                             //Set visible panels
                game.setVisible(false);
                upgrade.setVisible(false);
                menu.setVisible(true);
                options.setVisible(false);
                game.reset();                                                                                           //Reset the game
                Player.setWeapon_type(0);                                                                                 //Reset player weapon
                while (getFlag()[0]/*menu flag*/) {                                                                          //Wait for the menu screen to end
                    Thread.sleep(1);
                    menu.repaint();
                }
            }
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
            /**
             *        _____    _ _____   _____ _____            _____  ______    _____  _____ _____  ______ ______ _   _
             *       / / / |  | |  __ \ / ____|  __ \     /\   |  __ \|  ____|  / ____|/ ____|  __ \|  ____|  ____| \ | |
             *      / / /| |  | | |__) | |  __| |__) |   /  \  | |  | | |__    | (___ | |    | |__) | |__  | |__  |  \| |
             *     / / / | |  | |  ___/| | |_ |  _  /   / /\ \ | |  | |  __|    \___ \| |    |  _  /|  __| |  __| | . ` |
             *    / / /  | |__| | |    | |__| | | \ \  / ____ \| |__| | |____   ____) | |____| | \ \| |____| |____| |\  |
             *   /_/_/    \____/|_|     \_____|_|  \_\/_/    \_\_____/|______| |_____/ \_____|_|  \_\______|______|_| \_|
             *
             *
             * */
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
            if (getFlag()[4]/*upgrade flag*/) {
                GameScreen.setLevel(GameScreen.getLevel() + 1);
                gameOver.setVisible(false);
                game.setVisible(false);
                menu.setVisible(false);
                upgrade.setVisible(true);
                options.setVisible(false);
                game.reset();
                while (getFlag()[4]) {
                    Thread.sleep(1);
                    upgrade.repaint();
                    Player.reinitialiseStats();
                }
            }
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

            /**       __________ _   _ _____     _____  _____ _____  ______ ______ _   _
             *       / / /  ____| \ | |  __ \   / ____|/ ____|  __ \|  ____|  ____| \ | |
             *      / / /| |__  |  \| | |  | | | (___ | |    | |__) | |__  | |__  |  \| |
             *     / / / |  __| | . ` | |  | |  \___ \| |    |  _  /|  __| |  __| | . ` |
             *    / / /  | |____| |\  | |__| |  ____) | |____| | \ \| |____| |____| |\  |
             *   /_/_/   |______|_| \_|_____/  |_____/ \_____|_|  \_\______|______|_| \_|
             *
             *
             **/
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
            if (getFlag()[6]/*game_over flag*/) {                                                                            //This loop handles the game over screen
                game.setVisible(false);
                upgrade.setVisible(false);
                menu.setVisible(false);
                gameOver.setVisible(true);
                options.setVisible(false);
                while (getFlag()[6]/*game_over flag*/) {
                    gameOver.repaint();
                }
            }
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//

            /**       ________  _____ _______ _____ ____  _   _  _____    _____  _____ _____  ______ ______ _   _
             *       / / / __ \|  __ \__   __|_   _/ __ \| \ | |/ ____|  / ____|/ ____|  __ \|  ____|  ____| \ | |
             *      / / / |  | | |__) | | |    | || |  | |  \| | (___   | (___ | |    | |__) | |__  | |__  |  \| |
             *     / / /| |  | |  ___/  | |    | || |  | | . ` |\___ \   \___ \| |    |  _  /|  __| |  __| | . ` |
             *    / / / | |__| | |      | |   _| || |__| | |\  |____) |  ____) | |____| | \ \| |____| |____| |\  |
             *   /_/_/   \____/|_|      |_|  |_____\____/|_| \_|_____/  |_____/ \_____|_|  \_\______|______|_| \_|
             *
             **/
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
            if (getFlag()[1]/*options flag*/) {
                game.setVisible(false);
                upgrade.setVisible(false);
                menu.setVisible(false);
                gameOver.setVisible(false);
                options.setVisible(true);
                while (getFlag()[1]) {
                    options.repaint();
                }
            }
        }
    }

    public static boolean[] getFlag() {
        return Flag;
    }

    public static int getSoundtrack() {
        return soundtrack;
    }

    public static void setSoundtrack(int soundtrack) {
        Main.soundtrack = soundtrack;
    }

    private static boolean isLeftKey() {
        return leftKey;
    }

    public static void setLeftKey(boolean leftKey) {
        Main.leftKey = leftKey;
    }

    private static boolean isRightKey() {
        return rightKey;
    }

    public static void setRightKey(boolean rightKey) {
        Main.rightKey = rightKey;
    }

    private static boolean isShoot() {
        return shoot;
    }

    public static void setShoot(boolean shoot) {
        Main.shoot = shoot;
    }

    public static boolean isSpawn() {
        return spawn;
    }

    public static void setSpawn(boolean spawn) {
        Main.spawn = spawn;
    }

    public static int getStart() {
        return start;
    }

    public static void setStart(int start) {
        Main.start = start;
    }

    private static int getCount() {
        return count;
    }

    private static void setCount(int count) {
        Main.count = count;
    }

    public static void resetFlags(){
        Main.getFlag()[0] = false;
        Main.getFlag()[1] = false;
        Main.getFlag()[3] = false;
        Main.getFlag()[4] = false;
        Main.getFlag()[6] = false;
    }

    private static class PlayListener implements ActionListener {                                                       //This is a listener for any begin game buttons
        @Override
        public void actionPerformed(ActionEvent e) {
            resetFlags();
            getFlag()[3] = true;
            GameScreen.enemyInit();
            GameScreen.setMode(false);
        }
    }

    private static class LoadListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            resetFlags();
            getFlag()[3] = true;
            GameScreen.enemyInit();
            try {
                GameScreen.loadState();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            GameScreen.setMode(false);
        }
    }

    private static class ExitListener implements ActionListener {                                                       //This is a listener for returning to the main menu
        @Override
        public void actionPerformed(ActionEvent e) {
            getFlag()[4] = false;
            getFlag()[0] = true;
            getFlag()[3] = false;
            getFlag()[6] = false;
            getFlag()[1] = false;
        }
    }

    private static class OptListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            getFlag()[4] = false;
            getFlag()[0] = false;
            getFlag()[3] = false;
            getFlag()[1] = true;
            getFlag()[6] = false;
        }
    }
}

//This is the function that initialises the splash screen and runs it.
class Initialise implements Runnable {

    @Override
    public void run() {
        //Add the intro screen
        Intro intro = new Intro();
        //Initialise the splash screen
        try {
            intro.init();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Set bounds of splash screen
        intro.setSize(600, 600);
        //Set Screen layout as a card layout
        intro.setLayout(new CardLayout());
        //Show splash screen
        intro.setVisible(true);
        for (int i = 0; i < 90; i++) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            intro.repaint();
        }
        //when timeout occurs set the screen invisible
        intro.setVisible(false);
        intro.dispose();
    }
}



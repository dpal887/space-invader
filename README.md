   _____                          ____                     __
  / ___/____  ____ _________     /  _/___ _   ______ _____/ /__  __________
  \__ \/ __ \/ __ `/ ___/ _ \    / // __ \ | / / __ `/ __  / _ \/ ___/ ___/
 ___/ / /_/ / /_/ / /__/  __/  _/ // / / / |/ / /_/ / /_/ /  __/ /  (__  )
/____/ .___/\__,_/\___/\___/  /___/_/ /_/|___/\__,_/\__,_/\___/_/  /____/
    /_/
  _____                      _
 |_   _|__  __ _ _ __ ___   / |
   | |/ _ \/ _` | '_ ` _ \  | |
   | |  __/ (_| | | | | | | | |
   |_|\___|\__,_|_| |_| |_| |_|

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
++  NAME            :       Student ID      :       UPI             :   Profession       ++
++  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
++  DANIEL PALEY    :       6833863         :       dpal887         :   Computer Systems ++
++  FINN KENNEDY    :       1576845         :       fken005         :   Computer Systems ++
++  SUNNY WANG      :       5426465         :       hwan531         :   Computer Systems ++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Compiling:
ECLIPSE
>>Open Eclipse
>>Click Import Project
>>Select the project folder
>>Load file and press run
>>If prompted select run as java application
>>Select Screen Size from pop up window
>>Play game

JAR
>>Locate JAR file in <folder>/space-invader.jar
>>Double Click JAR File


How to play:
>>After hitting run, or double clicking the jar file a menu will pop up asking what screen resolution to use: 600x800 or 800x600.
>>!!IMPORTANT!!: Choose a screen size that fits your computer's resolution.
>>Press the OK button
>>Main menu should pop up after splash screen finishes loading
>>Select play from the options
>>Play game. The aim is to shoot down all of the invaders
    ||  Key - bindings:
    ||********In Game*****************
    ||                  S   - Shoot/Resume game/Start game
    ||                  A   - Move Left
    ||                  D   - Move Right
    ||                  P   - Pause Game
    ||********Other Keys**************
    ||                  C   - continue from game over screen or return to menu
    ||                  Esc - End game and exit from application
    ||                  pgUp   - Cheat to move straight to upgrade menu (round completed)
    ||                  pgDown - Move straight to game over screen/Suicide (round completed)
>> If you loose you will see a game over screen... Press C to return to main menu. As of current you will need to restart the game to re-initialise the barriers
>> If you win you will go to an upgrade screen where you can use your points to buy upgrades. Hit play to continue and see how far you can get...


                       PERCENTS - COMMENTING<c>
CLASS               DANIEL      FINN        HECHEN
----------------------------------------------------
Barrier             30%         70%-c       0%
Bullet              20%         80%-c       0%
Enemy               50%-c       50%         0%
GameOverScreen      80%         20%-c       0%
GameScreen          48.5%-c     48.5%-c     3%
Tutorial            100%-c      0%          0%
Heavy_Bullet        100%        0%-c        0%
Intro               100%-c      0%          0%
Lazer Bullet        100%        0%-c        0%
Main                80%-c       20%         0%
MainMenu            100%-c      0%          0%
MouseOver           100%-c      0%          0%
OptionsScreen       100%        0%-c        0%
Player              50%         50%-c       0%
PowerUp             0%          100%-c      0%
Screen              100%-c      0%          0%
ScreenSelect        100%-c      0%          0%
SoundMgr            100%-c      0%          0%
Spread_Bullet       100%        0%-c        0%
Standard_Bullet     100%        0%-c        0%
UpgradeScreen       90%-c       10%         0%

